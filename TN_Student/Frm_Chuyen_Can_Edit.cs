﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.Miscellaneous;
using TN.Students;
using TN.Studients;

namespace TN_Student
{
    public partial class Frm_Chuyen_Can_Edit : Form
    {
        public Frm_Chuyen_Can_Edit()
        {
            InitializeComponent();
            GV_Chuyen_Can_Layout();
            btn_View.Click += Btn_View_Click;
            GV_ChuyenCan.CellClick += GV_ChuyenCan_CellClick;
            GV_ChuyenCan.CellValueChanged += GV_ChuyenCan_CellValueChanged;
            GV_ChuyenCan.CellMouseUp += GV_ChuyenCan_CellMouseUp;
            btn_Save.Click += Btn_Save_Click;
            cbo_Department.SelectedValueChanged += Cbo_Department_SelectedValueChanged;
            cbo_Class.SelectedIndexChanged += Cbo_Class_SelectedIndexChanged;
        }


        private void Chuyen_Can_Edit_Load(object sender, EventArgs e)
        {
            LoadDataToToolbox.ComboBoxData(cbo_Department, "SELECT DepartmentKey,DepartmentName FROM HRM_Departments  WHERE RecordStatus!=99", "---Chọn Khoa---");

        }

        #region[Gidview]
        private void GV_Chuyen_Can_Layout()
        {
            DataGridViewCheckBoxColumn zColumn;

            // Setup Column 
            GV_ChuyenCan.Columns.Add("No", "STT");
            GV_ChuyenCan.Columns.Add("StudentID", "Mã sinh viên");
            GV_ChuyenCan.Columns.Add("StudentName", "Họ và tên");

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "All";
            zColumn.HeaderText = "Tất cả";
            GV_ChuyenCan.Columns.Add(zColumn);

            for (int i = 3; i < 18; i++)
            {
                zColumn = new DataGridViewCheckBoxColumn();
                zColumn.Name = "Session_" + (i - 2).ToString();
                zColumn.HeaderText = "Tuần" + (i - 2).ToString();
                GV_ChuyenCan.Columns.Add(zColumn);
            }
            GV_ChuyenCan.Columns.Add("Yes", "Có mặt");


            GV_ChuyenCan.Columns["No"].Width = 40;
            GV_ChuyenCan.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_ChuyenCan.Columns["StudentID"].Width = 120;
            GV_ChuyenCan.Columns["StudentID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_ChuyenCan.Columns["StudentID"].Frozen = true;

            GV_ChuyenCan.Columns["StudentName"].Width = 170;
            GV_ChuyenCan.Columns["StudentName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_ChuyenCan.Columns["StudentName"].Frozen = true;

            GV_ChuyenCan.Columns["All"].Width = 60;
            GV_ChuyenCan.Columns["All"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_ChuyenCan.Columns["All"].Frozen = true;

            for (int i = 3; i < 18; i++)
            {
                GV_ChuyenCan.Columns["Session_" + (i - 2).ToString()].Width = 60;
                GV_ChuyenCan.Columns["Session_" + (i - 2).ToString()].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            GV_ChuyenCan.Columns["Yes"].Width = 60;
            GV_ChuyenCan.Columns["Yes"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;


            GV_ChuyenCan.BackgroundColor = Color.White;
            GV_ChuyenCan.GridColor = Color.FromArgb(227, 239, 255);
            GV_ChuyenCan.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_ChuyenCan.DefaultCellStyle.ForeColor = Color.Navy;
            GV_ChuyenCan.DefaultCellStyle.Font = new Font("Arial", 9);
            GV_ChuyenCan.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 86, 189);
            GV_ChuyenCan.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(206, 246, 255);
            GV_ChuyenCan.EnableHeadersVisualStyles = false;

            GV_ChuyenCan.AllowUserToResizeRows = false;
            GV_ChuyenCan.AllowUserToResizeColumns = true;

            GV_ChuyenCan.RowHeadersVisible = false;
            GV_ChuyenCan.AllowUserToAddRows = false;
            GV_ChuyenCan.AllowUserToDeleteRows = false;

            //// setup Height Header
            // GV_Products.ColumnHeadersHeight = GV_Products.ColumnHeadersHeight * 3 / 2;
            GV_ChuyenCan.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_ChuyenCan.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;


        }
        private void GV_ChuyenCan_LoadData()
        {
            DateTime zDate = dte_Year.Value;
            DateTime zFromDate = new DateTime(dte_Year.Value.Year, 1, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(dte_Year.Value.Year, 12, 31, 23, 59, 59);
            int zDepartmentKey = cbo_Department.SelectedValue.ToInt();
            int zClass = cbo_Class.SelectedValue.ToInt();
            int zStudy = cbo_Study.SelectedValue.ToInt();

            GV_ChuyenCan.Rows.Clear();
            DataTable zTable = Order_Studys_Data.List_ChuyenCan(zFromDate, zToDate, zDepartmentKey, zClass, zStudy);

            int i = 0;

            foreach (DataRow nRow in zTable.Rows)
            {
                int h = 0;
                int zYes = 0;
                GV_ChuyenCan.Rows.Add();
                DataGridViewRow nRowView = GV_ChuyenCan.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Tag = nRow["AutoKey"].ToString().Trim();
                nRowView.Cells["StudentID"].Value = nRow["StudentID"].ToString().Trim();
                nRowView.Cells["StudentName"].Value = nRow["StudentName"].ToString().Trim();
                for (int k = 0; k < 15; k++)
                {
                    if (nRow["Session_" + (k + 1)].ToString() == "1")
                    {
                        nRowView.Cells["Session_" + (k + 1)].Value = true;
                        h++;
                        zYes++;
                    }
                    else
                    {
                        nRowView.Cells["Session_" + (k + 1)].Value = false;
                    }
                }
                if (h > 0)
                {
                    nRowView.Cells["All"].Value = true;
                }
                else
                {
                    nRowView.Cells["All"].Value = false;
                }
                nRowView.Cells["Yes"].Value = zYes;
                i++;
            }
        }
        private void GV_ChuyenCan_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void GV_ChuyenCan_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GV_ChuyenCan.Rows[e.RowIndex];
            if (e.ColumnIndex == 3 && e.RowIndex != -1)
            {
                if (Convert.ToBoolean(zRowEdit.Cells[e.ColumnIndex].Value) == false)
                {
                    for (int i = 3; i < 18; i++)
                    {
                        zRowEdit.Cells["Session_" + (i - 2)].Value = true;
                    }
                }
                else
                {
                    for (int i = 3; i < 18; i++)
                    {
                        zRowEdit.Cells["Session_" + (i - 2)].Value = false;
                    }
                }
            }
        }
        private void GV_ChuyenCan_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            GV_ChuyenCan.EndEdit();
        }
        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (GV_ChuyenCan.Rows.Count > 0)
            {
                Save_ChuyenCan();
            }
            else
            {
                MessageBox.Show("Không tìm thấy dữ liệu!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void Btn_View_Click(object sender, EventArgs e)
        {
            if (CheckBeforView() == true)
                GV_ChuyenCan_LoadData();
        }
        private void Cbo_Department_SelectedValueChanged(object sender, EventArgs e)
        {
            int ztemp = cbo_Department.SelectedValue.ToInt();
            LoadDataToToolbox.ComboBoxData(cbo_Class, "SELECT ClassKey,ClassName  FROM dbo.HRM_Class WHERE DepartmentKey= " + ztemp + " AND RecordStatus!=99 ", "---Chọn Lớp---");
        }
        private void Cbo_Class_SelectedIndexChanged(object sender, EventArgs e)
        {
            int ztemp = cbo_Class.SelectedValue.ToInt();
            LoadDataToToolbox.ComboBoxData(cbo_Study, "SELECT StudyKey,StudyName  FROM dbo.HRM_Order_Studys WHERE ClassKey= " + ztemp + " AND RecordStatus!=99 GROUP BY StudyKey,StudyName ", "---Chọn môn học---");
        }
        #endregion

        #region[Progess]
        private bool CheckBeforView()
        {
            bool zResult = true;
            if (cbo_Department.SelectedValue.ToInt() == 0)
            {
                MessageBox.Show("Vui lòng chọn Khoa!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                zResult = false;
            }
            else if (cbo_Class.SelectedValue.ToInt() == 0)
            {
                MessageBox.Show("Vui lòng chọn Lớp!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                zResult = false;
            }
            else if (cbo_Study.SelectedValue.ToInt() == 0)
            {
                MessageBox.Show("Vui lòng chọn Môn học!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                zResult = false;
            }
            return zResult;
        }
        private void Save_ChuyenCan()
        {
            string zMessage = "";
            int n = GV_ChuyenCan.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                if (GV_ChuyenCan.Rows[i].Tag != null)
                {
                    int zKey = GV_ChuyenCan.Rows[i].Tag.ToInt();
                    Order_Study_Detail_Info zAccess_Role = new Order_Study_Detail_Info(zKey);
                    if (GV_ChuyenCan.Rows[i].Cells["Session_1"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_ChuyenCan.Rows[i].Cells["Session_1"].Value) == true)
                        {
                            zAccess_Role.Session_1 = 1;
                        }
                        else
                        {
                            zAccess_Role.Session_1 = 0;
                        }
                    }
                    if (GV_ChuyenCan.Rows[i].Cells["Session_2"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_ChuyenCan.Rows[i].Cells["Session_2"].Value) == true)
                        {
                            zAccess_Role.Session_2 = 1;
                        }
                        else
                        {
                            zAccess_Role.Session_2 = 0;
                        }
                    }
                    if (GV_ChuyenCan.Rows[i].Cells["Session_3"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_ChuyenCan.Rows[i].Cells["Session_3"].Value) == true)
                        {
                            zAccess_Role.Session_3 = 1;
                        }
                        else
                        {
                            zAccess_Role.Session_3 = 0;
                        }
                    }
                    if (GV_ChuyenCan.Rows[i].Cells["Session_4"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_ChuyenCan.Rows[i].Cells["Session_4"].Value) == true)
                        {
                            zAccess_Role.Session_4 = 1;
                        }
                        else
                        {
                            zAccess_Role.Session_4 = 0;
                        }
                    }
                    if (GV_ChuyenCan.Rows[i].Cells["Session_5"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_ChuyenCan.Rows[i].Cells["Session_5"].Value) == true)
                        {
                            zAccess_Role.Session_5 = 1;
                        }
                        else
                        {
                            zAccess_Role.Session_5 = 0;
                        }
                    }
                    if (GV_ChuyenCan.Rows[i].Cells["Session_6"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_ChuyenCan.Rows[i].Cells["Session_6"].Value) == true)
                        {
                            zAccess_Role.Session_6 = 1;
                        }
                        else
                        {
                            zAccess_Role.Session_6 = 0;
                        }
                    }
                    if (GV_ChuyenCan.Rows[i].Cells["Session_7"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_ChuyenCan.Rows[i].Cells["Session_7"].Value) == true)
                        {
                            zAccess_Role.Session_7 = 1;
                        }
                        else
                        {
                            zAccess_Role.Session_7 = 0;
                        }
                    }
                    if (GV_ChuyenCan.Rows[i].Cells["Session_8"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_ChuyenCan.Rows[i].Cells["Session_8"].Value) == true)
                        {
                            zAccess_Role.Session_8 = 1;
                        }
                        else
                        {
                            zAccess_Role.Session_8 = 0;
                        }
                    }
                    if (GV_ChuyenCan.Rows[i].Cells["Session_9"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_ChuyenCan.Rows[i].Cells["Session_9"].Value) == true)
                        {
                            zAccess_Role.Session_9 = 1;
                        }
                        else
                        {
                            zAccess_Role.Session_9 = 0;
                        }
                    }
                    if (GV_ChuyenCan.Rows[i].Cells["Session_10"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_ChuyenCan.Rows[i].Cells["Session_10"].Value) == true)
                        {
                            zAccess_Role.Session_10 = 1;
                        }
                        else
                        {
                            zAccess_Role.Session_10 = 0;
                        }
                    }
                    if (GV_ChuyenCan.Rows[i].Cells["Session_11"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_ChuyenCan.Rows[i].Cells["Session_11"].Value) == true)
                        {
                            zAccess_Role.Session_11 = 1;
                        }
                        else
                        {
                            zAccess_Role.Session_11 = 0;
                        }
                    }
                    if (GV_ChuyenCan.Rows[i].Cells["Session_12"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_ChuyenCan.Rows[i].Cells["Session_12"].Value) == true)
                        {
                            zAccess_Role.Session_12 = 1;
                        }
                        else
                        {
                            zAccess_Role.Session_12 = 0;
                        }
                    }
                    if (GV_ChuyenCan.Rows[i].Cells["Session_13"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_ChuyenCan.Rows[i].Cells["Session_13"].Value) == true)
                        {
                            zAccess_Role.Session_13 = 1;
                        }
                        else
                        {
                            zAccess_Role.Session_13 = 0;
                        }
                    }
                    if (GV_ChuyenCan.Rows[i].Cells["Session_14"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_ChuyenCan.Rows[i].Cells["Session_14"].Value) == true)
                        {
                            zAccess_Role.Session_14 = 1;
                        }
                        else
                        {
                            zAccess_Role.Session_14 = 0;
                        }
                    }
                    if (GV_ChuyenCan.Rows[i].Cells["Session_15"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_ChuyenCan.Rows[i].Cells["Session_15"].Value) == true)
                        {
                            zAccess_Role.Session_15 = 1;
                        }
                        else
                        {
                            zAccess_Role.Session_15 = 0;
                        }
                    }
                    zAccess_Role.Update();
                    if (zAccess_Role.Message == "11" || zAccess_Role.Message == "20")
                        zMessage += "";
                    else
                        zMessage += "ERR";
                }
            }
            if (zMessage == "")
            {
                MessageBox.Show("Cập nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                GV_ChuyenCan_LoadData();
            }
            else
            {
                MessageBox.Show("Lỗi!Vui lòng liên hệ IT!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

    }
}
