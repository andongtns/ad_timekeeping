﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Students;

namespace TN_Student
{
    public partial class Frm_Branchs : Form
    {
        private int _Key=0;
        public Frm_Branchs()
        {
            InitializeComponent();
            LV_Layout(LV_List);
            btn_Save.Click += Btn_Save_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_New.Click += Btn_New_Click;
            btn_Search.Click += Btn_Search_Click;
            LV_List.Click += LV_List_Click;
        }
        private void Frm_Branchs_Load(object sender, EventArgs e)
        {
            LV_LoadData();
            LoadData();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            LV_LoadData();
        }

        private void Btn_New_Click(object sender, EventArgs e)
        {
            _Key = 0;
            LoadData();
        }

        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == 0)
            {
                MessageBox.Show("Chưa chọn thông tin!");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Branchs_Info zinfo = new Branchs_Info(_Key);
                    zinfo.Delete();
                    if (zinfo.Message == "30")
                    {
                        MessageBox.Show("Đã xóa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LV_LoadData();
                        _Key = 0;
                        LoadData();
                    }
                    else
                    {
                        MessageBox.Show("Lỗi!Vui lòng liên hệ IT!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_BranchID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập mã đơn vị!");
                return;
            }
            if (txt_BranchName.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập tên đơn vị!");
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!");
                return;
            }
            else
            {
                Branchs_Info zinfo = new Branchs_Info(_Key);
                zinfo.BranchID = txt_BranchID.Text.Trim().ToUpper();
                zinfo.BranchName = txt_BranchName.Text.Trim();
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                zinfo.Description = txt_Description.Text.Trim();
                zinfo.Save();
                if (zinfo.Message.Substring(0, 2) == "20" || zinfo.Message.Substring(0, 2) == "11")
                {
                    MessageBox.Show("Cập nhật thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LV_LoadData();
                    _Key = zinfo.Key;
                    LoadData();
                }
                else
                {
                    MessageBox.Show("Lỗi!Vui lòng liên hệ IT!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void LV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Phòng Ban";
            colHead.Width =200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

        }
        private void LV_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            DataTable zTable = Branchs_Data.Search(txt_Search.Text.Trim());
            for (int i = 0;i<zTable.Rows.Count;i++)
            {
                DataRow nRow = zTable.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["BranchKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["BranchID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["BranchName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
            this.Cursor = Cursors.Default;
        }

        private void LoadData()
        {
            Branchs_Info zInfo = new Branchs_Info(_Key);
            txt_BranchID.Text = zInfo.BranchID;
            txt_BranchName.Text = zInfo.BranchName;
            txt_Rank.Text = zInfo.Rank.ToString();
            txt_Description.Text = zInfo.Description;
        }
        private void LV_List_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < LV_List.Items.Count; i++)
            {
                if (LV_List.Items[i].Selected == true)
                {
                    LV_List.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LV_List.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _Key = int.Parse(LV_List.SelectedItems[0].Tag.ToString());
            LoadData();
        }


    }
}
