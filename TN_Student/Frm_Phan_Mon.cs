﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.Miscellaneous;
using TN.Students;
using TN.Studients;

namespace TN_Student
{
    public partial class Frm_Phan_Mon : Form
    {
        private int _Key = 0;
        public Frm_Phan_Mon()
        {
            InitializeComponent();
            LV_Layout(LV_Order);
            LV_Order.Click += LV_Order_Click;
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            cbo_Department.SelectedValueChanged += Cbo_Department_SelectedValueChanged;
        }

        private void Phan_Mon_Load(object sender, EventArgs e)
        {
            LoadDataToToolbox.ComboBoxData(cbo_Department, "SELECT DepartmentKey,DepartmentName FROM HRM_Departments WHERE RecordStatus !=99", "---Chọn Khoa---");
            LoadDataToToolbox.ComboBoxData(cbo_Class, "SELECT ClassKey,ClassName FROM HRM_Class WHERE RecordStatus !=99", "---Chọn Lớp---");
            LoadDataToToolbox.ComboBoxData(cbo_Study, "SELECT StudyKey,StudyName FROM HRM_Studys WHERE RecordStatus !=99", "---Chọn môn học---");
            LoadDataToToolbox.ComboBoxData(cbo_Department_Search, "SELECT DepartmentKey,DepartmentName FROM HRM_Departments WHERE RecordStatus !=99", "---Chọn Khoa---");
            LoadDataToToolbox.ComboBoxData(cbo_Class_Search, "SELECT ClassKey,ClassName FROM HRM_Class WHERE RecordStatus !=99", "---Chọn Lớp---");
            LoadDataToToolbox.ComboBoxData(cbo_Study_Search, "SELECT StudyKey,StudyName FROM HRM_Studys WHERE RecordStatus !=99", "---Chọn môn học---");
            LV_LoadData();
        }
       
        #region ListView
        private void LV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Khoa";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Lớp";
            colHead.Width = 90;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Môn";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ghi chú";
            colHead.Width = 300;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        public void LV_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Order;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            In_Table = Order_Studys_Data.List();

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["OrderKey"].ToString();
                lvi.BackColor = Color.White; ;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["DepartmentName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ClassName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["StudyName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["OrderDescription"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }
        private void LV_Order_Click(object sender, EventArgs e)
        {
            _Key = int.Parse(LV_Order.SelectedItems[0].Tag.ToString());
            LoadData();
        }
        #endregion

        #region[Progess]
        private void LoadData()
        {
            Order_Studys_Info zinfo = new Order_Studys_Info(_Key);
            if (zinfo.OrderDate == DateTime.MinValue)
                dte_Year.Value = DateTime.Now;
            else
            dte_Year.Value = zinfo.OrderDate;
            cbo_Department.SelectedValue = zinfo.DepartmentKey;
            cbo_Class.SelectedValue = zinfo.ClassKey;
            cbo_Study.SelectedValue = zinfo.StudyKey;
            txt_Description.Text = zinfo.OrderDescription;
        }

        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            string zResult = "";
            if (cbo_Department.SelectedValue.ToInt() == 0)
            {
                MessageBox.Show("Vui lòng chọn khoa!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (cbo_Class.SelectedValue.ToInt() == 0)
            {
                MessageBox.Show("Vui lòng chọn lớp!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (cbo_Study.SelectedValue.ToInt() == 0)
            {
                MessageBox.Show("Vui lòng chọn môn học!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                Order_Studys_Info zinfo = new Order_Studys_Info(_Key);
                zinfo.DepartmentKey = cbo_Department.SelectedValue.ToInt();
                zinfo.DepartmentName = cbo_Department.Text;
                zinfo.ClassKey = cbo_Class.SelectedValue.ToInt();
                zinfo.ClassName = cbo_Class.Text;
                zinfo.StudyKey = cbo_Class.SelectedValue.ToInt();
                zinfo.StudyName = cbo_Study.Text;
                zinfo.OrderDescription = txt_Description.Text;
                zinfo.OrderDate = dte_Year.Value;
                zinfo.Save();
                if (zinfo.Message.Substring(0, 2) == "11")
                {
                    _Key = zinfo.Key;
                    zinfo.Message = "";
                    zResult += zinfo.Message;
                    DataTable ztb = Order_Studys_Data.ListStudent_OfClass(zinfo.ClassKey);
                    foreach (DataRow row in ztb.Rows)
                    {
                        Order_Study_Detail_Info zDetail = new Order_Study_Detail_Info();
                        zDetail.OrderKey = zinfo.Key;
                        zDetail.StudentKey = row[0].ToString();
                        zDetail.StudentID = row[1].ToString();
                        zDetail.StudentName = row[2].ToString();
                        zDetail.Save();
                        if (zDetail.Message == "11" || zDetail.Message == "20")
                        {
                            zDetail.Message = "";
                            zResult += zDetail.Message;
                        }
                        else
                        {
                            zResult += zDetail.Message;
                        }

                    }

                }
                else if (zinfo.Message.Substring(0, 2) == "20")
                {
                    zinfo.Message = "";
                    zResult += zinfo.Message;
                }
                else
                {
                    zResult += zinfo.Message;
                }
                if (zResult == "")
                {
                    _Key = zinfo.Key;
                    LV_LoadData();
                    LoadData();
                    MessageBox.Show("Cập nhật thành công!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Lỗi!Vui lòng liên hệ IT!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }


        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            _Key = 0;
            LoadData();
        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key != 0)
            {
                Order_Studys_Info zinfo = new Order_Studys_Info(_Key);
                zinfo.Delete();
                if (zinfo.Message.Substring(0, 2) == "30")
                {
                    MessageBox.Show("Xóa thành công!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _Key = 0;
                    LV_LoadData();
                    LoadData();
                }
                else
                {
                    MessageBox.Show("Lỗi!Vui lòng liên hệ IT!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }
        private void Cbo_Department_SelectedValueChanged(object sender, EventArgs e)
        {
            int ztemp = cbo_Department.SelectedValue.ToInt();
            LoadDataToToolbox.ComboBoxData(cbo_Class, "SELECT ClassKey,ClassName  FROM dbo.HRM_Class WHERE DepartmentKey= " + ztemp + " AND RecordStatus!=99 ", "---Chọn Lớp---");
        }
        #endregion

    }
}

