﻿namespace TN_Student
{
    partial class Frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Main));
            this.MenuMain = new System.Windows.Forms.MenuStrip();
            this.MenuMain_System = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_System_ConnectDatabase = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMain_Category = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_Branch = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_Department = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_Class = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_Student = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_Study = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMain_Study = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMain_Diem_Danh = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMain_Chuyen_Can = new System.Windows.Forms.ToolStripMenuItem();
            this.chuyen_can = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMain_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatusLogin = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblDataBase = new System.Windows.Forms.ToolStripStatusLabel();
            this.MenuMain.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuMain
            // 
            this.MenuMain.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MenuMain.BackgroundImage")));
            this.MenuMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MenuMain.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuMain_System,
            this.MenuMain_Category,
            this.MenuMain_Study,
            this.MenuMain_Diem_Danh,
            this.MenuMain_Chuyen_Can,
            this.MenuMain_Exit});
            this.MenuMain.Location = new System.Drawing.Point(0, 0);
            this.MenuMain.Name = "MenuMain";
            this.MenuMain.Size = new System.Drawing.Size(925, 32);
            this.MenuMain.TabIndex = 1;
            this.MenuMain.Text = "Phần Mềm Quản Lý Chuyên Cần Sinh Viên";
            // 
            // MenuMain_System
            // 
            this.MenuMain_System.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_System_ConnectDatabase});
            this.MenuMain_System.ForeColor = System.Drawing.Color.Navy;
            this.MenuMain_System.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuMain_System.Name = "MenuMain_System";
            this.MenuMain_System.Size = new System.Drawing.Size(72, 28);
            this.MenuMain_System.Text = "Hệ thống";
            // 
            // Menu_System_ConnectDatabase
            // 
            this.Menu_System_ConnectDatabase.ForeColor = System.Drawing.Color.Navy;
            this.Menu_System_ConnectDatabase.Name = "Menu_System_ConnectDatabase";
            this.Menu_System_ConnectDatabase.Size = new System.Drawing.Size(157, 22);
            this.Menu_System_ConnectDatabase.Text = "Kết Nối CSDL";
            // 
            // MenuMain_Category
            // 
            this.MenuMain_Category.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_Branch,
            this.btn_Department,
            this.btn_Class,
            this.btn_Student,
            this.btn_Study});
            this.MenuMain_Category.ForeColor = System.Drawing.Color.Navy;
            this.MenuMain_Category.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuMain_Category.Name = "MenuMain_Category";
            this.MenuMain_Category.Size = new System.Drawing.Size(83, 28);
            this.MenuMain_Category.Text = "Danh mục ";
            // 
            // btn_Branch
            // 
            this.btn_Branch.ForeColor = System.Drawing.Color.Navy;
            this.btn_Branch.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btn_Branch.Name = "btn_Branch";
            this.btn_Branch.Size = new System.Drawing.Size(180, 22);
            this.btn_Branch.Text = "Đơn vị công tác";
            // 
            // btn_Department
            // 
            this.btn_Department.ForeColor = System.Drawing.Color.Navy;
            this.btn_Department.Name = "btn_Department";
            this.btn_Department.Size = new System.Drawing.Size(180, 22);
            this.btn_Department.Text = "Phòng ban";
            // 
            // btn_Class
            // 
            this.btn_Class.ForeColor = System.Drawing.Color.Navy;
            this.btn_Class.Name = "btn_Class";
            this.btn_Class.Size = new System.Drawing.Size(180, 22);
            this.btn_Class.Text = "Lớp học";
            // 
            // btn_Student
            // 
            this.btn_Student.ForeColor = System.Drawing.Color.Navy;
            this.btn_Student.Name = "btn_Student";
            this.btn_Student.Size = new System.Drawing.Size(180, 22);
            this.btn_Student.Text = "Sinh viên";
            // 
            // btn_Study
            // 
            this.btn_Study.ForeColor = System.Drawing.Color.Navy;
            this.btn_Study.Name = "btn_Study";
            this.btn_Study.Size = new System.Drawing.Size(180, 22);
            this.btn_Study.Text = "Môn học";
            // 
            // MenuMain_Study
            // 
            this.MenuMain_Study.ForeColor = System.Drawing.Color.Navy;
            this.MenuMain_Study.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuMain_Study.Name = "MenuMain_Study";
            this.MenuMain_Study.Size = new System.Drawing.Size(127, 28);
            this.MenuMain_Study.Text = "Phân môn cho lớp";
            this.MenuMain_Study.Click += new System.EventHandler(this.MenuMain_Study_Click);
            // 
            // MenuMain_Diem_Danh
            // 
            this.MenuMain_Diem_Danh.ForeColor = System.Drawing.Color.Navy;
            this.MenuMain_Diem_Danh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuMain_Diem_Danh.Name = "MenuMain_Diem_Danh";
            this.MenuMain_Diem_Danh.Size = new System.Drawing.Size(82, 28);
            this.MenuMain_Diem_Danh.Text = "Điểm danh";
            // 
            // MenuMain_Chuyen_Can
            // 
            this.MenuMain_Chuyen_Can.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chuyen_can});
            this.MenuMain_Chuyen_Can.ForeColor = System.Drawing.Color.Navy;
            this.MenuMain_Chuyen_Can.Image = ((System.Drawing.Image)(resources.GetObject("MenuMain_Chuyen_Can.Image")));
            this.MenuMain_Chuyen_Can.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuMain_Chuyen_Can.Name = "MenuMain_Chuyen_Can";
            this.MenuMain_Chuyen_Can.Size = new System.Drawing.Size(113, 28);
            this.MenuMain_Chuyen_Can.Text = "Chuyên cần";
            // 
            // chuyen_can
            // 
            this.chuyen_can.ForeColor = System.Drawing.Color.Navy;
            this.chuyen_can.Name = "chuyen_can";
            this.chuyen_can.Size = new System.Drawing.Size(209, 22);
            this.chuyen_can.Text = "Danh sách chuyên cần";
            // 
            // MenuMain_Exit
            // 
            this.MenuMain_Exit.ForeColor = System.Drawing.Color.Navy;
            this.MenuMain_Exit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuMain_Exit.Name = "MenuMain_Exit";
            this.MenuMain_Exit.Size = new System.Drawing.Size(52, 28);
            this.MenuMain_Exit.Text = "Thoát";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusLogin,
            this.lblDataBase});
            this.statusStrip1.Location = new System.Drawing.Point(0, 410);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(925, 24);
            this.statusStrip1.TabIndex = 17;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatusLogin
            // 
            this.lblStatusLogin.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblStatusLogin.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.lblStatusLogin.ForeColor = System.Drawing.Color.Navy;
            this.lblStatusLogin.Name = "lblStatusLogin";
            this.lblStatusLogin.Size = new System.Drawing.Size(65, 19);
            this.lblStatusLogin.Text = "Tình trạng";
            // 
            // lblDataBase
            // 
            this.lblDataBase.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblDataBase.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.lblDataBase.ForeColor = System.Drawing.Color.Navy;
            this.lblDataBase.Name = "lblDataBase";
            this.lblDataBase.Size = new System.Drawing.Size(48, 19);
            this.lblDataBase.Text = "CSDL : ";
            // 
            // Frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TN_Student.Properties.Resources._30415690_1641898885901823_3084867898588453156_n;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(925, 434);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.MenuMain);
            this.Name = "Frm_Main";
            this.Text = "Phần Mềm Quản Lý Chuyên Cần Sinh Viên";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_Main_Load);
            this.MenuMain.ResumeLayout(false);
            this.MenuMain.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuMain;
        private System.Windows.Forms.ToolStripMenuItem MenuMain_System;
        private System.Windows.Forms.ToolStripMenuItem Menu_System_ConnectDatabase;
        private System.Windows.Forms.ToolStripMenuItem MenuMain_Category;
        private System.Windows.Forms.ToolStripMenuItem btn_Branch;
        private System.Windows.Forms.ToolStripMenuItem btn_Department;
        private System.Windows.Forms.ToolStripMenuItem btn_Class;
        private System.Windows.Forms.ToolStripMenuItem btn_Student;
        private System.Windows.Forms.ToolStripMenuItem MenuMain_Study;
        private System.Windows.Forms.ToolStripMenuItem MenuMain_Diem_Danh;
        private System.Windows.Forms.ToolStripMenuItem MenuMain_Chuyen_Can;
        private System.Windows.Forms.ToolStripMenuItem chuyen_can;
        private System.Windows.Forms.ToolStripMenuItem MenuMain_Exit;
        private System.Windows.Forms.ToolStripMenuItem btn_Study;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatusLogin;
        private System.Windows.Forms.ToolStripStatusLabel lblDataBase;
    }
}