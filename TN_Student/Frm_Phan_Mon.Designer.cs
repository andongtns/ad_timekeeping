﻿namespace TN_Student
{
    partial class Frm_Phan_Mon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Phan_Mon));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btn_Save = new System.Windows.Forms.ToolStripButton();
            this.btn_New = new System.Windows.Forms.ToolStripButton();
            this.btn_Del = new System.Windows.Forms.ToolStripButton();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.txt_Search = new System.Windows.Forms.TextBox();
            this.cbo_Study_Search = new System.Windows.Forms.ComboBox();
            this.cbo_Class_Search = new System.Windows.Forms.ComboBox();
            this.cbo_Department_Search = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_Search = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.LV_Order = new System.Windows.Forms.ListView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_Description = new System.Windows.Forms.TextBox();
            this.cbo_Study = new System.Windows.Forms.ComboBox();
            this.cbo_Class = new System.Windows.Forms.ComboBox();
            this.cbo_Department = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Description = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dte_Year = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolStrip1.BackgroundImage")));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_Save,
            this.btn_New,
            this.btn_Del});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(889, 25);
            this.toolStrip1.TabIndex = 77;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btn_Save
            // 
            this.btn_Save.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.ForeColor = System.Drawing.Color.Navy;
            this.btn_Save.Image = ((System.Drawing.Image)(resources.GetObject("btn_Save.Image")));
            this.btn_Save.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btn_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(46, 22);
            this.btn_Save.Text = "Lưu";
            // 
            // btn_New
            // 
            this.btn_New.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_New.ForeColor = System.Drawing.Color.Navy;
            this.btn_New.Image = ((System.Drawing.Image)(resources.GetObject("btn_New.Image")));
            this.btn_New.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_New.Name = "btn_New";
            this.btn_New.Size = new System.Drawing.Size(70, 22);
            this.btn_New.Text = "Tạo Mới";
            // 
            // btn_Del
            // 
            this.btn_Del.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Del.ForeColor = System.Drawing.Color.Navy;
            this.btn_Del.Image = ((System.Drawing.Image)(resources.GetObject("btn_Del.Image")));
            this.btn_Del.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Del.Name = "btn_Del";
            this.btn_Del.Size = new System.Drawing.Size(47, 22);
            this.btn_Del.Text = "Xóa";
            // 
            // groupBox13
            // 
            this.groupBox13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.groupBox13.Controls.Add(this.txt_Search);
            this.groupBox13.Controls.Add(this.cbo_Study_Search);
            this.groupBox13.Controls.Add(this.cbo_Class_Search);
            this.groupBox13.Controls.Add(this.cbo_Department_Search);
            this.groupBox13.Controls.Add(this.label8);
            this.groupBox13.Controls.Add(this.label4);
            this.groupBox13.Controls.Add(this.label9);
            this.groupBox13.Controls.Add(this.btn_Search);
            this.groupBox13.Controls.Add(this.label5);
            this.groupBox13.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox13.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox13.Location = new System.Drawing.Point(460, 28);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(429, 183);
            this.groupBox13.TabIndex = 153;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Tìm kiếm";
            // 
            // txt_Search
            // 
            this.txt_Search.Location = new System.Drawing.Point(97, 118);
            this.txt_Search.Multiline = true;
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.Size = new System.Drawing.Size(215, 24);
            this.txt_Search.TabIndex = 162;
            // 
            // cbo_Study_Search
            // 
            this.cbo_Study_Search.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_Study_Search.ForeColor = System.Drawing.Color.Navy;
            this.cbo_Study_Search.FormattingEnabled = true;
            this.cbo_Study_Search.Location = new System.Drawing.Point(97, 85);
            this.cbo_Study_Search.Name = "cbo_Study_Search";
            this.cbo_Study_Search.Size = new System.Drawing.Size(215, 23);
            this.cbo_Study_Search.TabIndex = 161;
            // 
            // cbo_Class_Search
            // 
            this.cbo_Class_Search.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_Class_Search.ForeColor = System.Drawing.Color.Navy;
            this.cbo_Class_Search.FormattingEnabled = true;
            this.cbo_Class_Search.Location = new System.Drawing.Point(97, 52);
            this.cbo_Class_Search.Name = "cbo_Class_Search";
            this.cbo_Class_Search.Size = new System.Drawing.Size(215, 23);
            this.cbo_Class_Search.TabIndex = 161;
            // 
            // cbo_Department_Search
            // 
            this.cbo_Department_Search.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_Department_Search.ForeColor = System.Drawing.Color.Navy;
            this.cbo_Department_Search.FormattingEnabled = true;
            this.cbo_Department_Search.Location = new System.Drawing.Point(97, 20);
            this.cbo_Department_Search.Name = "cbo_Department_Search";
            this.cbo_Department_Search.Size = new System.Drawing.Size(215, 23);
            this.cbo_Department_Search.TabIndex = 161;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(13, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 15);
            this.label8.TabIndex = 160;
            this.label8.Text = "Khoa";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(12, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 15);
            this.label4.TabIndex = 142;
            this.label4.Text = "Tên && mã môn";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(12, 89);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 15);
            this.label9.TabIndex = 142;
            this.label9.Text = "Môn học";
            // 
            // btn_Search
            // 
            this.btn_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.btn_Search.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Search.BackgroundImage")));
            this.btn_Search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Search.FlatAppearance.BorderSize = 0;
            this.btn_Search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Search.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.ForeColor = System.Drawing.Color.White;
            this.btn_Search.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Search.Location = new System.Drawing.Point(332, 116);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(33, 27);
            this.btn_Search.TabIndex = 159;
            this.btn_Search.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_Search.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(12, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 15);
            this.label5.TabIndex = 142;
            this.label5.Text = "Lớp";
            // 
            // groupBox16
            // 
            this.groupBox16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.groupBox16.Controls.Add(this.LV_Order);
            this.groupBox16.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox16.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox16.Location = new System.Drawing.Point(0, 217);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(888, 247);
            this.groupBox16.TabIndex = 154;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Danh sách";
            // 
            // LV_Order
            // 
            this.LV_Order.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LV_Order.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Order.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Order.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LV_Order.FullRowSelect = true;
            this.LV_Order.GridLines = true;
            this.LV_Order.HideSelection = false;
            this.LV_Order.Location = new System.Drawing.Point(3, 17);
            this.LV_Order.Name = "LV_Order";
            this.LV_Order.Size = new System.Drawing.Size(882, 227);
            this.LV_Order.TabIndex = 149;
            this.LV_Order.UseCompatibleStateImageBehavior = false;
            this.LV_Order.View = System.Windows.Forms.View.Details;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.groupBox1.Controls.Add(this.dte_Year);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txt_Description);
            this.groupBox1.Controls.Add(this.cbo_Study);
            this.groupBox1.Controls.Add(this.cbo_Class);
            this.groupBox1.Controls.Add(this.cbo_Department);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Description);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox1.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox1.Location = new System.Drawing.Point(0, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(454, 183);
            this.groupBox1.TabIndex = 155;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin chi tiết";
            // 
            // txt_Description
            // 
            this.txt_Description.Location = new System.Drawing.Point(97, 140);
            this.txt_Description.Multiline = true;
            this.txt_Description.Name = "txt_Description";
            this.txt_Description.Size = new System.Drawing.Size(349, 37);
            this.txt_Description.TabIndex = 162;
            // 
            // cbo_Study
            // 
            this.cbo_Study.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_Study.ForeColor = System.Drawing.Color.Navy;
            this.cbo_Study.FormattingEnabled = true;
            this.cbo_Study.Location = new System.Drawing.Point(97, 107);
            this.cbo_Study.Name = "cbo_Study";
            this.cbo_Study.Size = new System.Drawing.Size(215, 23);
            this.cbo_Study.TabIndex = 161;
            // 
            // cbo_Class
            // 
            this.cbo_Class.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_Class.ForeColor = System.Drawing.Color.Navy;
            this.cbo_Class.FormattingEnabled = true;
            this.cbo_Class.Location = new System.Drawing.Point(97, 74);
            this.cbo_Class.Name = "cbo_Class";
            this.cbo_Class.Size = new System.Drawing.Size(215, 23);
            this.cbo_Class.TabIndex = 161;
            // 
            // cbo_Department
            // 
            this.cbo_Department.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_Department.ForeColor = System.Drawing.Color.Navy;
            this.cbo_Department.FormattingEnabled = true;
            this.cbo_Department.Location = new System.Drawing.Point(97, 42);
            this.cbo_Department.Name = "cbo_Department";
            this.cbo_Department.Size = new System.Drawing.Size(215, 23);
            this.cbo_Department.TabIndex = 161;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(13, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 15);
            this.label1.TabIndex = 160;
            this.label1.Text = "Khoa";
            // 
            // Description
            // 
            this.Description.AutoSize = true;
            this.Description.Font = new System.Drawing.Font("Arial", 9F);
            this.Description.ForeColor = System.Drawing.Color.Navy;
            this.Description.Location = new System.Drawing.Point(13, 148);
            this.Description.Name = "Description";
            this.Description.Size = new System.Drawing.Size(49, 15);
            this.Description.TabIndex = 142;
            this.Description.Text = "Ghi chú";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(12, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 15);
            this.label2.TabIndex = 142;
            this.label2.Text = "Môn học";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(12, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 15);
            this.label3.TabIndex = 142;
            this.label3.Text = "Lớp";
            // 
            // dte_Year
            // 
            this.dte_Year.CustomFormat = "yyyy";
            this.dte_Year.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_Year.Location = new System.Drawing.Point(97, 15);
            this.dte_Year.Name = "dte_Year";
            this.dte_Year.ShowUpDown = true;
            this.dte_Year.Size = new System.Drawing.Size(101, 21);
            this.dte_Year.TabIndex = 165;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(13, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 15);
            this.label6.TabIndex = 164;
            this.label6.Text = "Năm";
            // 
            // Frm_Phan_Mon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 474);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox16);
            this.Controls.Add(this.groupBox13);
            this.Controls.Add(this.toolStrip1);
            this.Name = "Frm_Phan_Mon";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phân bổ môn học cho các lớp học";
            this.Load += new System.EventHandler(this.Phan_Mon_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btn_Save;
        private System.Windows.Forms.ToolStripButton btn_New;
        private System.Windows.Forms.ToolStripButton btn_Del;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.ComboBox cbo_Study_Search;
        private System.Windows.Forms.ComboBox cbo_Class_Search;
        private System.Windows.Forms.ComboBox cbo_Department_Search;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.ListView LV_Order;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbo_Study;
        private System.Windows.Forms.ComboBox cbo_Class;
        private System.Windows.Forms.ComboBox cbo_Department;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_Search;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_Description;
        private System.Windows.Forms.Label Description;
        private System.Windows.Forms.DateTimePicker dte_Year;
        private System.Windows.Forms.Label label6;
    }
}