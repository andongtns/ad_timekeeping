﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.Connect;

namespace TN_Student
{
    public partial class Frm_Main : Form
    {
        private bool IsConnected = false;
        public Frm_Main()
        {
            InitializeComponent();
            //MenuMain_Study.Click += MenuMain_Study_Click;
            MenuMain_Diem_Danh.Click += MenuMain_Diem_Danh_Click;
            MenuMain_Exit.Click += MenuMain_Exit_Click;
            btn_Branch.Click += Btn_Branch_Click;
            Frm_Splash f = new Frm_Splash();
            f.ShowDialog();
            IsConnected = f.IsConnected;
            lblDataBase.Text = "CSDL : " + f.strDataBase;

            f.Dispose();
        }

        private void Btn_Branch_Click(object sender, EventArgs e)
        {
            Frm_Branchs frm = new Frm_Branchs();
            frm.Show();
        }

        private void MenuMain_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_Main_Load(object sender, EventArgs e)
        {
          
            if (!IsConnected)
            {
                //SetStatusMenu(false);
                lblStatusLogin.Text = "Tình trạng : Kết nối thất bại!  ";
                Menu_System_ConnectDatabase_Click(sender, null);
            }
            else
            {
                lblStatusLogin.Text = "Tình trạng : Kết nối thành công! ";

            }
        }
        private void Menu_System_ConnectDatabase_Click(object sender, EventArgs e)
        {
            Frm_ConnectSever frm = new Frm_ConnectSever();
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.Yes)
            {
                ConnectDatabase nConnect = new ConnectDatabase(frm.mConnectDataBaseInfo.ConnectionString);

                lblStatusLogin.Text = "Tình trạng : Kết nối thành công! ";


            }
        }

        private void MenuMain_Study_Click(object sender, EventArgs e)
        {
            Frm_Phan_Mon frm = new Frm_Phan_Mon();
                frm.ShowDialog();
        }
        private void MenuMain_Diem_Danh_Click(object sender, EventArgs e)
        {
            Frm_Chuyen_Can_Edit frm = new Frm_Chuyen_Can_Edit();
            frm.ShowDialog();
        }

    }
}
