﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.Connect;

namespace TN_Student
{
    public partial class Frm_ConnectSever : Form
    {
        public ConnectDataBaseInfo mConnectDataBaseInfo = new ConnectDataBaseInfo();
        string strMess01 = "Kết nối thành công";
        string strMess02 = "Không thể kết nối, vui lòng chọn kiểu kết nối khác ";
        string strMess03 = "Connecting...";
        string strMess04 = "Bạn phải chọn File Data ";
        public Frm_ConnectSever()
        {
            InitializeComponent();
            cmdConnect.Click += cmdConnect_Click;
        }

        private void Frm_ConnectSever_Load(object sender, EventArgs e)
        {
            RWConfig nFileConfig = new RWConfig();
            nFileConfig.ReadConfig();
            mConnectDataBaseInfo = nFileConfig.ConnectInfo;
            if (!mConnectDataBaseInfo.IsConnectLocal)
            {
                radioConnectServer.Checked = true;
                coSQLServer.Text = mConnectDataBaseInfo.DataSource;
                txtDataBase.Text = mConnectDataBaseInfo.DataBase;
                txtUserName.Text = mConnectDataBaseInfo.UserName;
                txtPass.Text = mConnectDataBaseInfo.Password;
            }
        }
        private void cmdConnect_Click(object sender, EventArgs e)
        {
            Connect();
        }
        private void Connect()
        {
            this.Cursor = Cursors.WaitCursor;
            lblMessage.Text = strMess03;

            if (CheckConnect())
            {

                this.DialogResult = DialogResult.Yes;
                this.Close();

            }
            else
            {
                lblMessage.Text = strMess02;
            }
            this.Cursor = Cursors.Default;
        }
        private bool CheckConnect()
        {

            mConnectDataBaseInfo = new ConnectDataBaseInfo();

            bool isConnected = false;

            if (radioConnectServer.Checked)
            {
                mConnectDataBaseInfo.DataSource = coSQLServer.Text;
                mConnectDataBaseInfo.DataBase = txtDataBase.Text;
                mConnectDataBaseInfo.UserName = txtUserName.Text;
                mConnectDataBaseInfo.Password = txtPass.Text;
                mConnectDataBaseInfo.IsConnectLocal = false;
                mConnectDataBaseInfo.AttachDbFilename = "Nofile";

                mConnectDataBaseInfo.ConnectionString = "Data Source = " + mConnectDataBaseInfo.DataSource + ";DataBase= " + mConnectDataBaseInfo.DataBase + ";User=" + mConnectDataBaseInfo.UserName + ";Password= " + mConnectDataBaseInfo.Password;// +";Integrated Security=SSPI";

            }
            SqlConnection nConSQL = new SqlConnection();
            try
            {


                nConSQL.ConnectionString = mConnectDataBaseInfo.ConnectionString;
                nConSQL.Open();

                if (nConSQL.State == ConnectionState.Open)
                {
                    isConnected = true;
                    MessageBox.Show(strMess01);
                    RWConfig nFileConfig = new RWConfig();
                    nFileConfig.ConnectInfo = mConnectDataBaseInfo;
                    nFileConfig.SaveConfig();
                }
            }
            catch (System.Exception err)
            {
                //   lbStatueConnect.Text = "Error Connect DataBase : " + err.Message;
                MessageBox.Show(err.Message);
                isConnected = false;
            }
            finally
            {
                nConSQL.Close();
            }
            return isConnected;
        }
    }
}
