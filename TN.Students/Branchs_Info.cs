﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Library.Connect;
using System.Data;


namespace TN.Students
{
    public class Branchs_Info
    {
        private int _BranchKey = 0;
        private string _BranchName = "";
        private string _BranchID = "";
        private int _Slug = 0;
        private int _Rank = 0;
        private int _RecordStatus = 0;
        private string _Description = "";
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreateName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";

        public int Key { get => _BranchKey; set => _BranchKey = value; }
        public string BranchName { get => _BranchName; set => _BranchName = value; }
        public string BranchID { get => _BranchID; set => _BranchID = value; }
        public int Slug { get => _Slug; set => _Slug = value; }
        public int Rank { get => _Rank; set => _Rank = value; }
        public int RecordStatus { get => _RecordStatus; set => _RecordStatus = value; }
        public string Description { get => _Description; set => _Description = value; }
        public DateTime CreatedOn { get => _CreatedOn; set => _CreatedOn = value; }
        public string CreatedBy { get => _CreatedBy; set => _CreatedBy = value; }
        public string CreateName { get => _CreateName; set => _CreateName = value; }
        public DateTime ModifiedOn { get => _ModifiedOn; set => _ModifiedOn = value; }
        public string ModifiedBy { get => _ModifiedBy; set => _ModifiedBy = value; }
        public string ModifiedName { get => _ModifiedName; set => _ModifiedName = value; }
        public string Message { get => _Message; set => _Message = value; }

        public Branchs_Info() { }
        public Branchs_Info(int BranchKey)
        {
            string zSQL = "SELECT * FROM HRM_Branchs WHERE BranchKey= @BranchKey";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    _BranchID = zReader["BranchID"].ToString();
                    _BranchName = zReader["BranchName"].ToString();
                    _Slug = int.Parse(zReader["Slug"].ToString());
                    _Rank = int.Parse(zReader["Rank"].ToString());
                    _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _Description = zReader["Description"].ToString();
                    _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreateName = zReader["CreateName"].ToString();
                    _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public string Create()
        {
            string zSQL = @"INSERT INTO HRM_Branchs 
                        ( BranchName, BranchID, Slug, Rank, RecordStatus, Description, CreatedOn, CreatedBy, CreateName, ModifiedOn, ModifiedBy, ModifiedName) 
                        VALUE
                        ( @BranchName, @BranchID, 0, @Rank, 0, @Description, Getdate(), '@CreatedBy', '@CreateName', Getdate(), '@ModifiedBy', '@ModifiedName')";
            zSQL += " SELECT '11' + CAST(BranchKey AS NVARCHAR) FROM HRM_Branch WHERE BranchKey = SCOPE_IDENTITY()";
            string zResult = "";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("BranchName", SqlDbType.NVarChar).Value = _BranchName;
                zCommand.Parameters.Add("BranchID", SqlDbType.NVarChar).Value = _BranchID;
                zCommand.Parameters.Add("Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("CreatedOn", SqlDbType.Date).Value = _CreatedOn;
                zCommand.Parameters.Add("CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("CreateName", SqlDbType.NVarChar).Value = _CreateName;
                zCommand.Parameters.Add("ModifiedOn", SqlDbType.Date).Value = _ModifiedOn;
                zCommand.Parameters.Add("ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteScalar().ToString();
                _Message = zResult.Substring(0, 2);
                if (_Message == "11")
                {
                    _BranchKey = int.Parse(zResult.Substring(2));
                }
                else
                {
                    _BranchKey = 0;
                }
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = @"UPDATE HRM_Branchs SET
                            BranchName= @BranchName, 
                            BranchID= @BranchID, 
                            Slug= @Slug, 
                            Rank= @Rank, 
                            RecordStatus= @RecordStatus, 
                            Description= @Description,
                            ModifiedOn= GETDATE(),
                            ModifiedBy= @ModifiedBy,
                            ModifiedName= @ModifiedName
                             WHERE BranchKey= @BranchKey";

            zSQL += " SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("BranchName", SqlDbType.NVarChar).Value = _BranchName;
                zCommand.Parameters.Add("BranchID", SqlDbType.NVarChar).Value = _BranchID;
                zCommand.Parameters.Add("Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zSQL = @"UPDATE HRM_Branchs SET 
                            RecordStatus= 99 ,
                            [ModifiedOn] = GETDATE(),
                            ModifiedBy = @ModifiedBy, 
                            ModifiedName = @ModifiedName
                            WHERE BranchKey= @BranchKey";
            zSQL += " SELECT 30";
            string zResult = "";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult = "";
            if (_BranchKey == 0)
            {
                zResult = Create();
            }
            else
            {
                zResult = Update();
            }
            return zResult;
        }
    }
}
