﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Library.Connect;

namespace TN.Students
{
    public class Branchs_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Branchs WHERE RecordStatus !=99 ";
            string zConnectionString = ConnectDatabase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch(Exception Err)
            {
                string Message = Err.ToString(); 
            }
            return zTable;
        }
        public static DataTable Search(string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Branchs WHERE (BranchName LIKE @Name OR BranchID LIKE @Name) AND   RecordStatus <> 99 ORDER BY [BranchName] DESC";
            string zConnectionString = ConnectDatabase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
