﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Library.Connect;

namespace TN.Studients
{
    public class Order_Study_Detail_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _OrderKey = 0;
        private string _OrderID = "";
        private string _StudentKey = "";
        private string _StudentID = "";
        private string _StudentName = "";
        private string _Description = "";
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private int _Session_1 = 0;
        private int _Session_2 = 0;
        private int _Session_3 = 0;
        private int _Session_4 = 0;
        private int _Session_5 = 0;
        private int _Session_6 = 0;
        private int _Session_7 = 0;
        private int _Session_8 = 0;
        private int _Session_9 = 0;
        private int _Session_10 = 0;
        private int _Session_11 = 0;
        private int _Session_12 = 0;
        private int _Session_13 = 0;
        private int _Session_14 = 0;
        private int _Session_15 = 0;
        private int _Session_16 = 0;
        private int _Session_17 = 0;
        private int _Session_18 = 0;
        private int _Session_19 = 0;
        private int _Session_20 = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private DateTime _ModifiedOn;
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int OrderKey
        {
            get { return _OrderKey; }
            set { _OrderKey = value; }
        }
        public string OrderID
        {
            get { return _OrderID; }
            set { _OrderID = value; }
        }
        public string StudentKey
        {
            get { return _StudentKey; }
            set { _StudentKey = value; }
        }
        public string StudentID
        {
            get { return _StudentID; }
            set { _StudentID = value; }
        }
        public string StudentName
        {
            get { return _StudentName; }
            set { _StudentName = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public int Session_1
        {
            get { return _Session_1; }
            set { _Session_1 = value; }
        }
        public int Session_2
        {
            get { return _Session_2; }
            set { _Session_2 = value; }
        }
        public int Session_3
        {
            get { return _Session_3; }
            set { _Session_3 = value; }
        }
        public int Session_4
        {
            get { return _Session_4; }
            set { _Session_4 = value; }
        }
        public int Session_5
        {
            get { return _Session_5; }
            set { _Session_5 = value; }
        }
        public int Session_6
        {
            get { return _Session_6; }
            set { _Session_6 = value; }
        }
        public int Session_7
        {
            get { return _Session_7; }
            set { _Session_7 = value; }
        }
        public int Session_8
        {
            get { return _Session_8; }
            set { _Session_8 = value; }
        }
        public int Session_9
        {
            get { return _Session_9; }
            set { _Session_9 = value; }
        }
        public int Session_10
        {
            get { return _Session_10; }
            set { _Session_10 = value; }
        }
        public int Session_11
        {
            get { return _Session_11; }
            set { _Session_11 = value; }
        }
        public int Session_12
        {
            get { return _Session_12; }
            set { _Session_12 = value; }
        }
        public int Session_13
        {
            get { return _Session_13; }
            set { _Session_13 = value; }
        }
        public int Session_14
        {
            get { return _Session_14; }
            set { _Session_14 = value; }
        }
        public int Session_15
        {
            get { return _Session_15; }
            set { _Session_15 = value; }
        }
        public int Session_16
        {
            get { return _Session_16; }
            set { _Session_16 = value; }
        }
        public int Session_17
        {
            get { return _Session_17; }
            set { _Session_17 = value; }
        }
        public int Session_18
        {
            get { return _Session_18; }
            set { _Session_18 = value; }
        }
        public int Session_19
        {
            get { return _Session_19; }
            set { _Session_19 = value; }
        }
        public int Session_20
        {
            get { return _Session_20; }
            set { _Session_20 = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Order_Study_Detail_Info()
        {
        }
        public Order_Study_Detail_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Order_Study_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["OrderKey"] != DBNull.Value)
                        _OrderKey = int.Parse(zReader["OrderKey"].ToString());
                    _OrderID = zReader["OrderID"].ToString().Trim();
                    _StudentKey = zReader["StudentKey"].ToString().Trim();
                    _StudentID = zReader["StudentID"].ToString().Trim();
                    _StudentName = zReader["StudentName"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["Session_1"] != DBNull.Value)
                        _Session_1 = int.Parse(zReader["Session_1"].ToString());
                    if (zReader["Session_2"] != DBNull.Value)
                        _Session_2 = int.Parse(zReader["Session_2"].ToString());
                    if (zReader["Session_3"] != DBNull.Value)
                        _Session_3 = int.Parse(zReader["Session_3"].ToString());
                    if (zReader["Session_4"] != DBNull.Value)
                        _Session_4 = int.Parse(zReader["Session_4"].ToString());
                    if (zReader["Session_5"] != DBNull.Value)
                        _Session_5 = int.Parse(zReader["Session_5"].ToString());
                    if (zReader["Session_6"] != DBNull.Value)
                        _Session_6 = int.Parse(zReader["Session_6"].ToString());
                    if (zReader["Session_7"] != DBNull.Value)
                        _Session_7 = int.Parse(zReader["Session_7"].ToString());
                    if (zReader["Session_8"] != DBNull.Value)
                        _Session_8 = int.Parse(zReader["Session_8"].ToString());
                    if (zReader["Session_9"] != DBNull.Value)
                        _Session_9 = int.Parse(zReader["Session_9"].ToString());
                    if (zReader["Session_10"] != DBNull.Value)
                        _Session_10 = int.Parse(zReader["Session_10"].ToString());
                    if (zReader["Session_11"] != DBNull.Value)
                        _Session_11 = int.Parse(zReader["Session_11"].ToString());
                    if (zReader["Session_12"] != DBNull.Value)
                        _Session_12 = int.Parse(zReader["Session_12"].ToString());
                    if (zReader["Session_13"] != DBNull.Value)
                        _Session_13 = int.Parse(zReader["Session_13"].ToString());
                    if (zReader["Session_14"] != DBNull.Value)
                        _Session_14 = int.Parse(zReader["Session_14"].ToString());
                    if (zReader["Session_15"] != DBNull.Value)
                        _Session_15 = int.Parse(zReader["Session_15"].ToString());
                    if (zReader["Session_16"] != DBNull.Value)
                        _Session_16 = int.Parse(zReader["Session_16"].ToString());
                    if (zReader["Session_17"] != DBNull.Value)
                        _Session_17 = int.Parse(zReader["Session_17"].ToString());
                    if (zReader["Session_18"] != DBNull.Value)
                        _Session_18 = int.Parse(zReader["Session_18"].ToString());
                    if (zReader["Session_19"] != DBNull.Value)
                        _Session_19 = int.Parse(zReader["Session_19"].ToString());
                    if (zReader["Session_20"] != DBNull.Value)
                        _Session_20 = int.Parse(zReader["Session_20"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Order_Study_Detail ("
        + " OrderKey ,OrderID ,StudentKey ,StudentID ,StudentNamee ,Description ,Slug ,RecordStatus ,Session_1 ,Session_2 ,Session_3 ,Session_4 ,Session_5 ,Session_6 ,Session_7 ,Session_8 ,Session_9 ,Session_10 ,Session_11 ,Session_12 ,Session_13 ,Session_14 ,Session_15 ,Session_16 ,Session_17 ,Session_18 ,Session_19 ,Session_20 ,CreatedBy ,CreatedName ,CreatedOn ,ModifiedBy ,ModifiedName ,ModifiedOn ) "
         + " VALUES ( "
         + "@OrderKey ,@OrderID ,@StudentKey ,@StudentID ,@StudentNamee ,@Description ,@Slug ,0 ,@Session_1 ,@Session_2 ,@Session_3 ,@Session_4 ,@Session_5 ,@Session_6 ,@Session_7 ,@Session_8 ,@Session_9 ,@Session_10 ,@Session_11 ,@Session_12 ,@Session_13 ,@Session_14 ,@Session_15 ,@Session_16 ,@Session_17 ,@Session_18 ,@Session_19 ,@Session_20 ,@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ,GETDATE()) ";
            zSQL += " SELECT '11' + CAST(AutoKey AS NVARCHAR) FROM dbo.HRM_Order_Study_Detail WHERE AutoKey = SCOPE_IDENTITY()";
            string zResult = "";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.Int).Value = _OrderKey;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID.Trim();
                zCommand.Parameters.Add("@StudentKey", SqlDbType.NVarChar).Value = _StudentKey.Trim();
                zCommand.Parameters.Add("@StudentID", SqlDbType.NVarChar).Value = _StudentID.Trim();
                zCommand.Parameters.Add("@StudentName", SqlDbType.NVarChar).Value = _StudentName.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@Session_1", SqlDbType.Int).Value = _Session_1;
                zCommand.Parameters.Add("@Session_2", SqlDbType.Int).Value = _Session_2;
                zCommand.Parameters.Add("@Session_3", SqlDbType.Int).Value = _Session_3;
                zCommand.Parameters.Add("@Session_4", SqlDbType.Int).Value = _Session_4;
                zCommand.Parameters.Add("@Session_5", SqlDbType.Int).Value = _Session_5;
                zCommand.Parameters.Add("@Session_6", SqlDbType.Int).Value = _Session_6;
                zCommand.Parameters.Add("@Session_7", SqlDbType.Int).Value = _Session_7;
                zCommand.Parameters.Add("@Session_8", SqlDbType.Int).Value = _Session_8;
                zCommand.Parameters.Add("@Session_9", SqlDbType.Int).Value = _Session_9;
                zCommand.Parameters.Add("@Session_10", SqlDbType.Int).Value = _Session_10;
                zCommand.Parameters.Add("@Session_11", SqlDbType.Int).Value = _Session_11;
                zCommand.Parameters.Add("@Session_12", SqlDbType.Int).Value = _Session_12;
                zCommand.Parameters.Add("@Session_13", SqlDbType.Int).Value = _Session_13;
                zCommand.Parameters.Add("@Session_14", SqlDbType.Int).Value = _Session_14;
                zCommand.Parameters.Add("@Session_15", SqlDbType.Int).Value = _Session_15;
                zCommand.Parameters.Add("@Session_16", SqlDbType.Int).Value = _Session_16;
                zCommand.Parameters.Add("@Session_17", SqlDbType.Int).Value = _Session_17;
                zCommand.Parameters.Add("@Session_18", SqlDbType.Int).Value = _Session_18;
                zCommand.Parameters.Add("@Session_19", SqlDbType.Int).Value = _Session_19;
                zCommand.Parameters.Add("@Session_20", SqlDbType.Int).Value = _Session_20;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteScalar().ToString();
                _Message = zResult.Substring(0, 2);
                if (_Message == "11")
                    _AutoKey = int.Parse(zResult.Substring(2));
                else
                    _AutoKey = 0;
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Order_Study_Detail SET "
                        + " OrderKey = @OrderKey,"
                        + " OrderID = @OrderID,"
                        + " StudentKey = @StudentKey,"
                        + " StudentID = @StudentID,"
                        + " StudentName = @StudentName,"
                        + " Description = @Description,"
                        + " Slug = @Slug,"
                        + " RecordStatus = @RecordStatus,"
                        + " Session_1 = @Session_1,"
                        + " Session_2 = @Session_2,"
                        + " Session_3 = @Session_3,"
                        + " Session_4 = @Session_4,"
                        + " Session_5 = @Session_5,"
                        + " Session_6 = @Session_6,"
                        + " Session_7 = @Session_7,"
                        + " Session_8 = @Session_8,"
                        + " Session_9 = @Session_9,"
                        + " Session_10 = @Session_10,"
                        + " Session_11 = @Session_11,"
                        + " Session_12 = @Session_12,"
                        + " Session_13 = @Session_13,"
                        + " Session_14 = @Session_14,"
                        + " Session_15 = @Session_15,"
                        + " Session_16 = @Session_16,"
                        + " Session_17 = @Session_17,"
                        + " Session_18 = @Session_18,"
                        + " Session_19 = @Session_19,"
                        + " Session_20 = @Session_20,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GETDATE()"
                       + " WHERE AutoKey = @AutoKey";
            zSQL += " SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.Int).Value = _OrderKey;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID.Trim();
                zCommand.Parameters.Add("@StudentKey", SqlDbType.NVarChar).Value = _StudentKey.Trim();
                zCommand.Parameters.Add("@StudentID", SqlDbType.NVarChar).Value = _StudentID.Trim();
                zCommand.Parameters.Add("@StudentName", SqlDbType.NVarChar).Value = _StudentName.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@Session_1", SqlDbType.Int).Value = _Session_1;
                zCommand.Parameters.Add("@Session_2", SqlDbType.Int).Value = _Session_2;
                zCommand.Parameters.Add("@Session_3", SqlDbType.Int).Value = _Session_3;
                zCommand.Parameters.Add("@Session_4", SqlDbType.Int).Value = _Session_4;
                zCommand.Parameters.Add("@Session_5", SqlDbType.Int).Value = _Session_5;
                zCommand.Parameters.Add("@Session_6", SqlDbType.Int).Value = _Session_6;
                zCommand.Parameters.Add("@Session_7", SqlDbType.Int).Value = _Session_7;
                zCommand.Parameters.Add("@Session_8", SqlDbType.Int).Value = _Session_8;
                zCommand.Parameters.Add("@Session_9", SqlDbType.Int).Value = _Session_9;
                zCommand.Parameters.Add("@Session_10", SqlDbType.Int).Value = _Session_10;
                zCommand.Parameters.Add("@Session_11", SqlDbType.Int).Value = _Session_11;
                zCommand.Parameters.Add("@Session_12", SqlDbType.Int).Value = _Session_12;
                zCommand.Parameters.Add("@Session_13", SqlDbType.Int).Value = _Session_13;
                zCommand.Parameters.Add("@Session_14", SqlDbType.Int).Value = _Session_14;
                zCommand.Parameters.Add("@Session_15", SqlDbType.Int).Value = _Session_15;
                zCommand.Parameters.Add("@Session_16", SqlDbType.Int).Value = _Session_16;
                zCommand.Parameters.Add("@Session_17", SqlDbType.Int).Value = _Session_17;
                zCommand.Parameters.Add("@Session_18", SqlDbType.Int).Value = _Session_18;
                zCommand.Parameters.Add("@Session_19", SqlDbType.Int).Value = _Session_19;
                zCommand.Parameters.Add("@Session_20", SqlDbType.Int).Value = _Session_20;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE HRM_Order_Study_Detail SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey";
            zSQL += " SELECT 30";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
