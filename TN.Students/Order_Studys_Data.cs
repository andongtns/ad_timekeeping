﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Library.Connect;

namespace TN.Students
{
    public class Order_Studys_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Order_Studys WHERE RecordStatus != 99 ";
            string zConnectionString = ConnectDatabase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListStudent_OfClass( int Class)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.StudentKey,A.StudentID,B.FullName FROM dbo.HRM_Students A
LEFT JOIN[dbo].[HRM_Student_Personals] B ON B.StudentKey =A.StudentKey
WHERE A.RecordStatus !=99 AND B.RecordStatus !=99 AND A.ClassKey = " + Class + "";

           string zConnectionString = ConnectDatabase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_ChuyenCan(DateTime FromDate, DateTime ToDate, int DepartmentKey,int ClassKey,int StudyKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.* FROM   dbo.HRM_Order_Study_Detail A 
LEFT JOIN dbo.HRM_Order_Studys B ON B.OrderKey =A.OrderKey 
WHERE B.OrderDate BETWEEN @FromDate AND @ToDate 
AND B.DepartmentKey = @DepartmentKey AND B.ClassKey= @ClassKey AND B.StudyKey= @StudyKey 
AND A.RecordStatus !=99 AND B.RecordStatus !=99 ";

            string zConnectionString = ConnectDatabase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@ClassKey", SqlDbType.Int).Value = ClassKey;
                zCommand.Parameters.Add("@StudyKey", SqlDbType.Int).Value = StudyKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
    }
}
