﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Library.Connect;

namespace TN.Studients
{
    public class Order_Studys_Info
    {

        #region [ Field Name ]
        private int _OrderKey = 0;
        private string _OrderID = "";
        private DateTime _OrderDate;
        private DateTime _OrderQuit;
        private int _ClassKey = 0;
        private string _ClassName = "";
        private int _StudyKey = 0;
        private string _StudyName = "";
        private int _DepartmentKey = 0;
        private string _DepartmentName = "";
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private DateTime _ModifiedOn;
        private string _RoleID = "";
        private string _Message = "";
        private string _OrderDescription = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _OrderKey; }
            set { _OrderKey = value; }
        }
        public string OrderID
        {
            get { return _OrderID; }
            set { _OrderID = value; }
        }
        public DateTime OrderDate
        {
            get { return _OrderDate; }
            set { _OrderDate = value; }
        }
        public DateTime OrderQuit
        {
            get { return _OrderQuit; }
            set { _OrderQuit = value; }
        }
        public int ClassKey
        {
            get { return _ClassKey; }
            set { _ClassKey = value; }
        }
        public string ClassName
        {
            get { return _ClassName; }
            set { _ClassName = value; }
        }
        public int StudyKey
        {
            get { return _StudyKey; }
            set { _StudyKey = value; }
        }
        public string StudyName
        {
            get { return _StudyName; }
            set { _StudyName = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string DepartmentName
        {
            get { return _DepartmentName; }
            set { _DepartmentName = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string OrderDescription
        {
            get
            {
                return _OrderDescription;
            }

            set
            {
                _OrderDescription = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Order_Studys_Info()
        {
        }
        public Order_Studys_Info(int OrderKey)
        {
            string zSQL = "SELECT * FROM HRM_Order_Studys WHERE OrderKey = @OrderKey";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.Int).Value = OrderKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["OrderKey"] != DBNull.Value)
                        _OrderKey = int.Parse(zReader["OrderKey"].ToString());
                    _OrderID = zReader["OrderID"].ToString().Trim();
                    if (zReader["OrderDate"] != DBNull.Value)
                        _OrderDate = (DateTime)zReader["OrderDate"];
                    if (zReader["OrderQuit"] != DBNull.Value)
                        _OrderQuit = (DateTime)zReader["OrderQuit"];
                    if (zReader["ClassKey"] != DBNull.Value)
                        _ClassKey = int.Parse(zReader["ClassKey"].ToString());
                    _ClassName = zReader["ClassName"].ToString().Trim();
                    if (zReader["StudyKey"] != DBNull.Value)
                        _StudyKey = int.Parse(zReader["StudyKey"].ToString());
                    _StudyName = zReader["StudyName"].ToString().Trim();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString().Trim());
                    _DepartmentName = zReader["DepartmentName"].ToString().Trim();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryName = zReader["CategoryName"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Order_Studys ("
        + " OrderID ,OrderDate ,OrderQuit ,ClassKey ,ClassName ,StudyKey ,StudyName ,DepartmentKey ,DepartmentName ,CategoryKey ,CategoryName ,OrderDescription,Slug ,RecordStatus ,CreatedBy ,CreatedName ,CreatedOn ,ModifiedBy ,ModifiedName ,ModifiedOn ) "
         + " VALUES ( "
         + "@OrderID ,@OrderDate ,@OrderQuit ,@ClassKey ,@ClassName ,@StudyKey ,@StudyName ,@DepartmentKey ,@DepartmentName ,@CategoryKey ,@CategoryName ,@OrderDescription,@Slug ,0 ,@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ,GETDATE()) ";
            zSQL += " SELECT '11' + CAST(OrderKey AS NVARCHAR) FROM dbo.HRM_Order_Studys WHERE OrderKey = SCOPE_IDENTITY()";
            string zResult = "";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID.Trim();
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                if (_OrderQuit == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderQuit", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderQuit", SqlDbType.DateTime).Value = _OrderQuit;
                zCommand.Parameters.Add("@ClassKey", SqlDbType.Int).Value = _ClassKey;
                zCommand.Parameters.Add("@ClassName", SqlDbType.NVarChar).Value = _ClassName.Trim();
                zCommand.Parameters.Add("@StudyKey", SqlDbType.Int).Value = _StudyKey;
                zCommand.Parameters.Add("@StudyName", SqlDbType.NVarChar).Value = _StudyName.Trim();
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = _DepartmentName.Trim();
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                zResult = zCommand.ExecuteScalar().ToString();
                _Message = zResult.Substring(0, 2);
                if (_Message == "11")
                    _OrderKey = int.Parse(zResult.Substring(2));
                else
                    _OrderKey = 0;
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Order_Studys SET "
                        + " OrderID = @OrderID,"
                        + " OrderDate = @OrderDate,"
                        + " OrderQuit = @OrderQuit,"
                        + " ClassKey = @ClassKey,"
                        + " ClassName = @ClassName,"
                        + " StudyKey = @StudyKey,"
                        + " StudyName = @StudyName,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " DepartmentName = @DepartmentName,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " OrderDescription =@OrderDescription,"
                        + " Slug = @Slug,"
                        + " RecordStatus =1,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GETDATE()"
                       + " WHERE OrderKey = @OrderKey";
            zSQL += " SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.Int).Value = _OrderKey;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID.Trim();
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                if (_OrderQuit == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderQuit", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderQuit", SqlDbType.DateTime).Value = _OrderQuit;
                zCommand.Parameters.Add("@ClassKey", SqlDbType.Int).Value = _ClassKey;
                zCommand.Parameters.Add("@ClassName", SqlDbType.NVarChar).Value = _ClassName.Trim();
                zCommand.Parameters.Add("@StudyKey", SqlDbType.Int).Value = _StudyKey;
                zCommand.Parameters.Add("@StudyName", SqlDbType.NVarChar).Value = _StudyName.Trim();
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = _DepartmentName.Trim();
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Parameters.Add("@OrderDescription", SqlDbType.NVarChar).Value = _OrderDescription;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE HRM_Order_Studys SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE OrderKey = @OrderKey 
                            UPDATE HRM_Order_Study_Detail SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE OrderKey = @OrderKey";
                   zSQL += " SELECT 30";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.Int).Value = _OrderKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_OrderKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
