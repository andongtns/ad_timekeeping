﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Library.Connect
{
    public class RWConfig
    {
        private string strFileName = Path.GetTempPath() + "students.dll";
        private string mKeys = "151019752801197824012008";
        private ConnectDataBaseInfo mConnectInfo;
        public RWConfig()
        {
            mConnectInfo = new ConnectDataBaseInfo();
        }
        public RWConfig(string strConnect, bool IsConnectLocal)
        {
            mConnectInfo = new ConnectDataBaseInfo();
            string[] strTemp;
            strTemp = strConnect.Split(';');
            if (IsConnectLocal)
            {
                mConnectInfo.DataSource = strTemp[0];
                mConnectInfo.AttachDbFilename = strTemp[1]; ;
            }
        }
        public ConnectDataBaseInfo ConnectInfo
        {
            get
            {
                return mConnectInfo;
            }
            set
            {
                mConnectInfo = value;
            }
        }

        public void SaveConfig()
        {
            WriteFile(HashStringConnectInfo(mConnectInfo));
        }
        public bool ReadConfig()
        {
            bool nResult = false;
            string strConnect = "";
            if (File.Exists(strFileName))
            {
                FileStream fBin = new FileStream(strFileName, FileMode.Open);

                fBin.Position = 0;
                BinaryReader nRead = new BinaryReader(fBin);

                strConnect = nRead.ReadString();

                nRead.Close();
                fBin.Close();
                nResult = true;
                mConnectInfo = InputInfo(UnHashStringConnectInfo(strConnect));
            }
            else
            {
                mConnectInfo = new ConnectDataBaseInfo();
                nResult = false;
            }
            return nResult;
        }

        private void WriteFile(string strConnectInfo)
        {
            string nResult = "";
            try
            {
                FileStream fBin = new FileStream(strFileName, FileMode.OpenOrCreate);
                BinaryWriter w = new BinaryWriter(fBin);

                w.Write(strConnectInfo);
                w.Flush();
                w.Close();
                fBin.Close();
            }
            catch (Exception ex)
            {
                nResult = ex.ToString();
            }

        }
        private string HashStringConnectInfo(ConnectDataBaseInfo ConnectInfo)
        {
            string nTemp = RandomString(5000);
            string nResult = "", strConnect = "", nSize;
            Random random = new Random();
            strConnect += ConnectInfo.DataSource + ";";
            strConnect += ConnectInfo.DataBase + ";";
            strConnect += ConnectInfo.UserName + ";";
            strConnect += ConnectInfo.Password + ";";
            strConnect += ConnectInfo.AttachDbFilename + ";";
            strConnect += ConnectInfo.IsConnectLocal.ToString() + ";";

            nSize = Convert.ToString(strConnect.Length);
            nSize = nSize.PadLeft(3, '0');

            strConnect = nSize + strConnect;

            int k = 0;
            for (int i = 0; i < strConnect.Length; i++)
            {
                nResult += strConnect[i].ToString();

                int nKey = int.Parse(mKeys.Substring(k, 2));
                nResult += nTemp.Substring(random.Next(4500), nKey);

                k = k + 2;
                if (k >= mKeys.Length)
                    k = 0;
            }


            return nResult;
        }
        private string UnHashStringConnectInfo(string strConnect)
        {
            string nResult = "";
            string nLenStrConnect = "";
            int nPosition = 0, k = 0;

            nLenStrConnect += strConnect[nPosition].ToString();
            //get length
            for (int i = 0; i < 2; i++)
            {
                nPosition += int.Parse(mKeys.Substring(k, 2)) + 1;
                nLenStrConnect += strConnect[nPosition].ToString();
                k = k + 2;
            }

            for (int i = 0; i < int.Parse(nLenStrConnect); i++)
            {
                nPosition += int.Parse(mKeys.Substring(k, 2)) + 1;
                nResult += strConnect[nPosition].ToString();
                k = k + 2;
                if (k >= mKeys.Length)
                    k = 0;
            }
            return nResult;
        }
        private ConnectDataBaseInfo InputInfo(string strConnect)
        {
            ConnectDataBaseInfo nConnect = new ConnectDataBaseInfo();
            string[] strTemp;
            strTemp = strConnect.Split(';');
            nConnect.DataSource = strTemp[0];
            nConnect.DataBase = strTemp[1];
            nConnect.UserName = strTemp[2];
            nConnect.Password = strTemp[3];
            nConnect.AttachDbFilename = strTemp[4];
            nConnect.IsConnectLocal = bool.Parse(strTemp[5]);

            return nConnect;
        }

        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder("");
            string nResult = "";
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(random.Next(30, 122)));
                nResult += ch.ToString();
            }
            return nResult;
        }
    }
}
