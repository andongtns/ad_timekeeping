﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Library.Connect
{
    public class ConnectDatabase
    {
        private static SqlConnection SQLConnect;
        private static string mConnectionString = "";
        private string mMessage = "";
        public ConnectDatabase()
        {
            SQLConnect = new SqlConnection();
        }

        public ConnectDatabase(string StrConnect)
        {
            try
            {
                SQLConnect = new SqlConnection();
                SQLConnect.ConnectionString = StrConnect;
                SQLConnect.Open();
                mConnectionString = StrConnect;
            }
            catch (Exception Err)
            {
                mMessage = Err.ToString();
            }
            finally
            {
                SQLConnect.Close();
            }

        }
        public void CloseConnect()
        {
            SQLConnect.Close();
        }
        public string Message
        {
            get
            {
                return mMessage;
            }
        }


        public static string ConnectionString
        {
            set
            {
                mConnectionString = value;
            }
            get
            {
                return mConnectionString;
            }

        }
        public static bool StillConnect
        {
            get
            {
                if (SQLConnect == null || SQLConnect.State == ConnectionState.Closed)
                    return false;
                else
                    return true;
            }
        }
    }
}
