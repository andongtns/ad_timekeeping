﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Library.Connect;
using TN.Library.System;

namespace TN.Library.System
{
    public class User_Info
    {
        private string _UserKey = "";
        private string _UserName = "";
        private string _Password = "";
        private string _Description = "";
        private int _RecordStatus = 0;

        private DateTime _ExpireDate = DateTime.Now.AddYears(1);
        private DateTime _LastLoginDate = DateTime.Now;
        private int _FailedPasswordAttemptCount = 0;
        private int _IsLock = 0;

        private int _EmployeeKey = 0;
        private string _EmployeeName = "";

        private string _CreatedBy = "";
        private DateTime _CreatedOn;
        private string _ModifiedBy = "";
        private DateTime _ModifiedOn;

        private string _Message = "";
        public User_Info()
        {
        }

        #region [ Constructor Get Information ]


        public User_Info(string UserName)
        {
            string zSQL = " SELECT A.*,B.* FROM SYS_User A "
                        + " LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey "
                        + " WHERE A.UserName = @UserName ";

            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = UserName;

                SqlDataReader nReader = zCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    _UserKey = nReader["UserKey"].ToString();
                    _UserName = nReader["UserName"].ToString();
                    _Password = nReader["Password"].ToString();
                    _Description = nReader["Description"].ToString();

                    _RecordStatus = (int)nReader["RecordStatus"];
                    _ExpireDate = (DateTime)nReader["ExpireDate"];

                    _EmployeeKey = int.Parse(nReader["EmployeeKey"].ToString());
                    _EmployeeName = nReader["FullName"].ToString();
                    if (nReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)nReader["LastLoginDate"];
                    _FailedPasswordAttemptCount = (int)nReader["FailedPasswordAttemptCount"];


                }

                //---- Close Connect SQL ----
                nReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        public void GetEmployee(string UserKey)
        {
            string zSQL = " SELECT * FROM SYS_User WHERE UserKey = @UserKey";

            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
                SqlDataReader nReader = zCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    _EmployeeKey = int.Parse(nReader["EmployeeKey"].ToString());
                    _UserKey = nReader["UserKey"].ToString();
                    _UserName = nReader["UserName"].ToString();
                    _Password = nReader["Password"].ToString();
                    _Description = nReader["Description"].ToString();

                    _RecordStatus = (int)nReader["RecordStatus"];
                    _ExpireDate = (DateTime)nReader["ExpireDate"];

                    if (nReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)nReader["LastLoginDate"];
                    if (nReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)nReader["CreatedOn"];
                    _FailedPasswordAttemptCount = (int)nReader["FailedPasswordAttemptCount"];
                }
                //---- Close Connect SQL ----
                nReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        public void GetUser(string UserKey)
        {
            string zSQL = " SELECT * FROM SYS_User WHERE UserKey = @UserKey";

            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
                SqlDataReader nReader = zCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    _UserKey = nReader["UserKey"].ToString();
                    _UserName = nReader["UserName"].ToString();
                    _Password = nReader["Password"].ToString();
                    _Description = nReader["Description"].ToString();

                    _RecordStatus = (int)nReader["RecordStatus"];
                    _ExpireDate = (DateTime)nReader["ExpireDate"];

                    _EmployeeKey = int.Parse(nReader["EmployeeKey"].ToString());

                    if (nReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)nReader["LastLoginDate"];

                    if (nReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)nReader["CreatedOn"];

                    _FailedPasswordAttemptCount = (int)nReader["FailedPasswordAttemptCount"];

                }
                //---- Close Connect SQL ----
                nReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [Properties ]

        public string Key
        {
            set { _UserKey = value; }
            get { return _UserKey; }
        }
        public string Name
        {
            set { _UserName = value; }
            get { return _UserName; }
        }
        public string Password
        {
            set
            {
                string nPassword = value;
                _Password = MyCryptography.HashPass(nPassword);
            }
            get { return _Password; }
        }
        public string Description
        {
            set { _Description = value; }
            get { return _Description; }
        }

        public DateTime ExpireDate
        {
            set { _ExpireDate = value; }
            get { return _ExpireDate; }
        }

        public DateTime LastLoginDate
        {
            set { _LastLoginDate = value; }
            get { return _LastLoginDate; }
        }
        public int FailedPasswordAttemptCount
        {
            set { _FailedPasswordAttemptCount = value; }
            get { return _FailedPasswordAttemptCount; }
        }

        public int EmployeeKey
        {
            set { _EmployeeKey = value; }
            get { return _EmployeeKey; }
        }
        public string EmployeeName
        {
            set { _EmployeeName = value; }
            get { return _EmployeeName; }
        }

        public string CreatedBy
        {
            set { _CreatedBy = value; }
            get { return _CreatedBy; }
        }
        public DateTime CreatedOn
        {
            set { _CreatedOn = value; }
            get { return _CreatedOn; }
        }

        public string ModifiedBy
        {
            set { _ModifiedBy = value; }
            get { return _ModifiedBy; }
        }
        public DateTime ModifiedOn
        {
            set { _ModifiedOn = value; }
            get { return _ModifiedOn; }
        }

        public string Message
        {
            set { _Message = value; }
            get { return _Message; }
        }

        public int RecordStatus { get => _RecordStatus; set => _RecordStatus = value; }

        public int IsLock
        {
            get
            {
                return _IsLock;
            }

            set
            {
                _IsLock = value;
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string UpdateFailedPass()
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_User SET "
                        + " FailedPasswordAttemptCount = FailedPasswordAttemptCount+1"
                        + " WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateFaildPassDefault()
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_User SET "
                        + " FailedPasswordAttemptCount = @FailedPasswordAttemptCount"
                        + " WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                zCommand.Parameters.Add("@FailedPasswordAttemptCount", SqlDbType.Int).Value = _FailedPasswordAttemptCount;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateDateLogin()
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_Users SET "
                        + " LastLoginDate = getdate(),"
                        + " FailedPasswordAttemptCount = 0"
                        + " WHERE UserKey = @UserKey";

            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdatePassWord()
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_User SET "
                        + " Password = @Password"
                        + " WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = _Password;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string nResult = ""; ;

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_User SET "
                        + " UserName = @UserName,"
                        + " Password = @Password,"
                        + " Description = @Description,"
                        + " RecordStatus = @RecordStatus,"
                        + " ExpireDate = @ExpireDate,"
                        + " EmployeeKey = @EmployeeKey, "
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedOn = getdate() "
                        + " WHERE UserKey = @UserKey";

            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = _UserName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = _Password;
                zCommand.Parameters.Add("@Description", SqlDbType.NText).Value = _Description;
                zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                if (_EmployeeKey != 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = 0;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                nResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return nResult;
        }
        public string Create()
        {

            string nResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = @"INSERT INTO SYS_User
          (
            UserName,
            Password,
            ExpireDate,
            RecordStatus,
            EmployeeKey,
            FailedPasswordAttemptCount,
            CreatedBy,
            CreatedOn
          ) VALUES(
             @UserName,
             @Password,
             @ExpireDate,
            @RecordStatus,
             @EmployeeKey,
            @FailedPasswordAttemptCount,
             @CreatedBy,
             GETDATE()
          )";

            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {

                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = _UserName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = _Password;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                if (_ExpireDate.Year == 001)
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@FailedPasswordAttemptCount", SqlDbType.Int).Value = _FailedPasswordAttemptCount;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                nResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return nResult;
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_User WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDatabase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                nResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return nResult;
        }

        public string Save()
        {
            if (_UserKey.Trim().Length > 0)
                return Update();
            else
                return Create();
        }
        #endregion
    }

}
