﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Library.System
{
    public class SessionUser
    {
        private static User_Info _UserLogin = new User_Info();
        private static DataTable _Table_Role = new DataTable();
        private static DateTime _Date_Work = DateTime.Now;
        private static int _StoreKey = 0;
        public SessionUser()
        {

        }
        public SessionUser(string UserName)
        {
            _UserLogin = new User_Info(UserName);
        }
        public SessionUser(string UserName, string UserKey)
        {
            _UserLogin = new User_Info(UserName);
            //_Table_Role = User_Data.UserRole_ID(UserKey);
        }

        public static User_Info UserLogin
        {
            get
            {
                return _UserLogin;
            }

        }

        public static DataTable Table_Role { get => _Table_Role; set => _Table_Role = value; }
        public static DateTime Date_Work { get => _Date_Work; set => _Date_Work = value; }

        public static int StoreKey
        {
            get
            {
                return _StoreKey;
            }

            set
            {
                _StoreKey = value;
            }
        }
    }
}
