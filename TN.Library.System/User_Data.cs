﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Library.Connect;

namespace TN.Library.System
{
    public class User_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_User ";
            string zConnectionString = ConnectDatabase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search(string UserName)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *  FROM SYS_User";
            if (UserName.Length > 0)
                zSQL += " WHERE UserName LIKE @UserName";
            string zConnectionString = ConnectDatabase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = UserName;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static string[] CheckUser(string UserName, string Pass)
        {
            string[] nResult = new string[2];
            User_Info nUserLogin = new User_Info(UserName);

            if (nUserLogin.Key.Trim().Length == 0)
            {
                nResult[0] = "ERR";
                nResult[1] = "CheckUser_Error01";
                return nResult;//"Don't have this UserName";
            }

            if (nUserLogin.RecordStatus == 52)
            {
                nResult[0] = "ERR";
                nResult[1] = "CheckUser_Error02";
                return nResult;//"Don't Activate"
            }

            if (nUserLogin.RecordStatus == 53)
            {
                nResult[0] = "ERR";
                nResult[1] = "CheckUser_Error03";
                return nResult;//"User IsLock"
            }

            if (nUserLogin.ExpireDate < DateTime.Now)
            {
                nResult[0] = "ERR";
                nResult[1] = "CheckUser_Error04";
                return nResult;//"Expire On"
            }

            if (nUserLogin.Password != MyCryptography.HashPass(Pass))
            {
                nUserLogin.UpdateFailedPass();
                nResult[0] = "ERR";
                nResult[1] = "CheckUser_Error05";
                return nResult;// "Wrong Password";
            }

            nResult[0] = "OK";
            nResult[1] = nUserLogin.Key;
            nUserLogin.UpdateDateLogin();
            return nResult;
        }
        public static DataTable GetStore(string UserKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"[dbo].[SYS_Access_Store_Data]";
            string zConnectionString = ConnectDatabase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
