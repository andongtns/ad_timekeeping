USE [master]
GO
/****** Object:  Database [TN_Student]    Script Date: 10/09/2019 10:49:37 ******/
CREATE DATABASE [TN_Student]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TN_Student', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.ANDONG\MSSQL\DATA\TN_Student.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'TN_Student_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.ANDONG\MSSQL\DATA\TN_Student_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [TN_Student] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TN_Student].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TN_Student] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TN_Student] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TN_Student] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TN_Student] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TN_Student] SET ARITHABORT OFF 
GO
ALTER DATABASE [TN_Student] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TN_Student] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TN_Student] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TN_Student] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TN_Student] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TN_Student] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TN_Student] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TN_Student] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TN_Student] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TN_Student] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TN_Student] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TN_Student] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TN_Student] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TN_Student] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TN_Student] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TN_Student] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TN_Student] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TN_Student] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TN_Student] SET  MULTI_USER 
GO
ALTER DATABASE [TN_Student] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TN_Student] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TN_Student] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TN_Student] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [TN_Student] SET DELAYED_DURABILITY = DISABLED 
GO
USE [TN_Student]
GO
/****** Object:  Table [dbo].[HRM_Branchs]    Script Date: 10/09/2019 10:49:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRM_Branchs](
	[BranchKey] [int] IDENTITY(1,1) NOT NULL,
	[BranchID] [nvarchar](50) NULL,
	[BranchName] [nvarchar](250) NULL,
	[Slug] [int] NULL,
	[Rank] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[RecordStatus] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedName] [nvarchar](250) NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedName] [nvarchar](250) NULL,
 CONSTRAINT [PK_SYS_Branchs] PRIMARY KEY CLUSTERED 
(
	[BranchKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRM_Class]    Script Date: 10/09/2019 10:49:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRM_Class](
	[ClassKey] [int] IDENTITY(1,1) NOT NULL,
	[ClassID] [nvarchar](50) NULL,
	[ClassName] [nvarchar](250) NULL,
	[DepartmentKey] [int] NULL,
	[DepartmentName] [nvarchar](250) NULL,
	[Rank] [int] NULL,
	[Slug] [int] NULL,
	[RecordStatus] [int] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedName] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedName] [nvarchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_HRM_Class] PRIMARY KEY CLUSTERED 
(
	[ClassKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRM_Departments]    Script Date: 10/09/2019 10:49:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRM_Departments](
	[DepartmentKey] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentName] [nvarchar](250) NULL,
	[DepartmentID] [nvarchar](50) NULL,
	[BranchKey] [int] NULL,
	[BranchName] [nvarchar](250) NULL,
	[Rank] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[Slug] [int] NULL,
	[RecordStatus] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedName] [nvarchar](250) NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedName] [nvarchar](250) NULL,
 CONSTRAINT [PK_SYS_Departments] PRIMARY KEY CLUSTERED 
(
	[DepartmentKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRM_Order_Study_Detail]    Script Date: 10/09/2019 10:49:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRM_Order_Study_Detail](
	[AutoKey] [int] IDENTITY(1,1) NOT NULL,
	[OrderKey] [int] NULL,
	[OrderID] [nvarchar](50) NULL,
	[StudentKey] [nvarchar](50) NULL,
	[StudentID] [nvarchar](50) NULL,
	[StudentName] [nvarchar](250) NULL,
	[Session_1] [int] NULL,
	[Session_2] [int] NULL,
	[Session_3] [int] NULL,
	[Session_4] [int] NULL,
	[Session_5] [int] NULL,
	[Session_6] [int] NULL,
	[Session_7] [int] NULL,
	[Session_8] [int] NULL,
	[Session_9] [int] NULL,
	[Session_10] [int] NULL,
	[Session_11] [int] NULL,
	[Session_12] [int] NULL,
	[Session_13] [int] NULL,
	[Session_14] [int] NULL,
	[Session_15] [int] NULL,
	[Session_16] [int] NULL,
	[Session_17] [int] NULL,
	[Session_18] [int] NULL,
	[Session_19] [int] NULL,
	[Session_20] [int] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Slug] [int] NULL,
	[RecordStatus] [int] NULL CONSTRAINT [DF_HRM_Order_Study_Detail_RecordStatus]  DEFAULT ((0)),
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedName] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedName] [nvarchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_HRM_Order_Study_Detail] PRIMARY KEY CLUSTERED 
(
	[AutoKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRM_Order_Studys]    Script Date: 10/09/2019 10:49:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRM_Order_Studys](
	[OrderKey] [int] IDENTITY(1,1) NOT NULL,
	[OrderID] [nvarchar](50) NULL,
	[OrderDate] [datetime] NULL,
	[OrderQuit] [datetime] NULL,
	[ClassKey] [int] NULL,
	[ClassName] [nvarchar](250) NULL,
	[StudyKey] [int] NULL,
	[StudyName] [nvarchar](250) NULL,
	[DepartmentKey] [int] NULL,
	[DepartmentName] [nvarchar](250) NULL,
	[CategoryKey] [int] NULL,
	[CategoryName] [nvarchar](150) NULL,
	[Slug] [int] NULL,
	[RecordStatus] [int] NULL,
	[OrderDescription] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedName] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedName] [nvarchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_HRM_Order_Studys] PRIMARY KEY CLUSTERED 
(
	[OrderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRM_Student_Personals]    Script Date: 10/09/2019 10:49:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRM_Student_Personals](
	[PersonalKey] [uniqueidentifier] NOT NULL CONSTRAINT [DF_HRM_Student_Personl_PersonalKey]  DEFAULT (newid()),
	[PersonID] [nvarchar](50) NULL,
	[FirstName] [nvarchar](100) NULL,
	[LastName] [nvarchar](100) NULL,
	[FullName] [nvarchar](100) NULL,
	[StudentKey] [nvarchar](50) NULL,
	[Gender] [int] NULL,
	[Marriage] [int] NULL,
	[BirthDay] [datetime] NULL,
	[HomeTown] [nvarchar](300) NULL,
	[IssueID] [nvarchar](50) NULL,
	[IssueDate] [datetime] NULL,
	[IssuePlace] [nvarchar](50) NULL,
	[Description] [nvarchar](300) NULL,
	[Phone] [nvarchar](50) NULL,
	[Email] [nvarchar](100) NULL,
	[BankCode] [nvarchar](50) NULL,
	[AddressBank] [nvarchar](150) NULL,
	[Slug] [int] NULL,
	[RecordStatus] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedName] [nvarchar](250) NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedName] [nvarchar](250) NULL,
 CONSTRAINT [PK_HRM_Employee_Personals] PRIMARY KEY CLUSTERED 
(
	[PersonalKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRM_Students]    Script Date: 10/09/2019 10:49:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRM_Students](
	[StudentKey] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Table_1_EmployeeKey]  DEFAULT (newid()),
	[StudentID] [nvarchar](50) NULL,
	[BranchKey] [int] NULL,
	[BranchName] [nvarchar](100) NULL,
	[DepartmentKey] [int] NULL,
	[DepartmentName] [nvarchar](100) NULL,
	[TeacherKey] [nvarchar](50) NULL,
	[TeacherName] [nvarchar](250) NULL,
	[PositionKey] [int] NULL,
	[PositionName] [nvarchar](100) NULL,
	[ClassKey] [int] NULL,
	[ClassName] [nvarchar](250) NULL,
	[BeginDate] [datetime] NULL,
	[QuitDate] [datetime] NULL,
	[Leaved] [int] NULL,
	[Description] [nvarchar](500) NULL,
	[Phone] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Slug] [int] NULL,
	[Rank] [int] NULL,
	[RecordStatus] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedName] [nvarchar](250) NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedName] [nvarchar](250) NULL,
 CONSTRAINT [PK_HRM_Students] PRIMARY KEY CLUSTERED 
(
	[StudentKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HRM_Studys]    Script Date: 10/09/2019 10:49:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HRM_Studys](
	[StudyKey] [int] IDENTITY(1,1) NOT NULL,
	[StudyID] [nvarchar](50) NULL,
	[StudyName] [nvarchar](250) NULL,
	[CategoryKey] [int] NULL,
	[CategoryName] [nvarchar](250) NULL,
	[Sesstion] [int] NULL,
	[Credit] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[Rank] [int] NULL,
	[Slug] [int] NOT NULL,
	[RecordStatus] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedName] [nvarchar](250) NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedName] [nvarchar](250) NULL,
 CONSTRAINT [PK_SYS_Studys] PRIMARY KEY CLUSTERED 
(
	[StudyKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[HRM_Branchs] ON 

INSERT [dbo].[HRM_Branchs] ([BranchKey], [BranchID], [BranchName], [Slug], [Rank], [Description], [RecordStatus], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1, N'DLA_LA', N'Đại học kinh tế công nghiệp long an _Tại Long An', 0, 0, N'', 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Branchs] ([BranchKey], [BranchID], [BranchName], [Slug], [Rank], [Description], [RecordStatus], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2, N'DLA_Q7', N'Văn phòng đại diện quận 7', 0, 0, N'', 0, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[HRM_Branchs] OFF
SET IDENTITY_INSERT [dbo].[HRM_Class] ON 

INSERT [dbo].[HRM_Class] ([ClassKey], [ClassID], [ClassName], [DepartmentKey], [DepartmentName], [Rank], [Slug], [RecordStatus], [CreatedBy], [CreatedName], [CreatedOn], [ModifiedBy], [ModifiedName], [ModifiedOn]) VALUES (1, N'', N'14TH', 1, N'Khoa kĩ thuật công nghệ', 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Class] ([ClassKey], [ClassID], [ClassName], [DepartmentKey], [DepartmentName], [Rank], [Slug], [RecordStatus], [CreatedBy], [CreatedName], [CreatedOn], [ModifiedBy], [ModifiedName], [ModifiedOn]) VALUES (2, N'', N'15TH', 1, N'Khoa kĩ thuật công nghệ', 2, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Class] ([ClassKey], [ClassID], [ClassName], [DepartmentKey], [DepartmentName], [Rank], [Slug], [RecordStatus], [CreatedBy], [CreatedName], [CreatedOn], [ModifiedBy], [ModifiedName], [ModifiedOn]) VALUES (3, N'', N'16TH', 1, N'Khoa kĩ thuật công nghệ', 3, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Class] ([ClassKey], [ClassID], [ClassName], [DepartmentKey], [DepartmentName], [Rank], [Slug], [RecordStatus], [CreatedBy], [CreatedName], [CreatedOn], [ModifiedBy], [ModifiedName], [ModifiedOn]) VALUES (4, N'', N'14TA', 2, N'Khoa tiếng anh', 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Class] ([ClassKey], [ClassID], [ClassName], [DepartmentKey], [DepartmentName], [Rank], [Slug], [RecordStatus], [CreatedBy], [CreatedName], [CreatedOn], [ModifiedBy], [ModifiedName], [ModifiedOn]) VALUES (5, N'', N'15TA', 2, N'Khoa tiếng anh', 2, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Class] ([ClassKey], [ClassID], [ClassName], [DepartmentKey], [DepartmentName], [Rank], [Slug], [RecordStatus], [CreatedBy], [CreatedName], [CreatedOn], [ModifiedBy], [ModifiedName], [ModifiedOn]) VALUES (6, N'', N'16TA', 2, N'Khoa tiếng anh', 3, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[HRM_Class] OFF
SET IDENTITY_INSERT [dbo].[HRM_Departments] ON 

INSERT [dbo].[HRM_Departments] ([DepartmentKey], [DepartmentName], [DepartmentID], [BranchKey], [BranchName], [Rank], [Description], [Slug], [RecordStatus], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1, N'Khoa kĩ thuật công nghệ', N'', 1, N'Đại học kinh tế công nghiệp long an _Tại Long An', 0, N'', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Departments] ([DepartmentKey], [DepartmentName], [DepartmentID], [BranchKey], [BranchName], [Rank], [Description], [Slug], [RecordStatus], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2, N'Khoa tiếng anh', N'', 1, N'Đại học kinh tế công nghiệp long an _Tại Long An', 1, N'', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Departments] ([DepartmentKey], [DepartmentName], [DepartmentID], [BranchKey], [BranchName], [Rank], [Description], [Slug], [RecordStatus], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (3, N'Khoa tài chính', N'', 1, N'Đại học kinh tế công nghiệp long an _Tại Long An', 2, N'', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[HRM_Departments] OFF
SET IDENTITY_INSERT [dbo].[HRM_Order_Study_Detail] ON 

INSERT [dbo].[HRM_Order_Study_Detail] ([AutoKey], [OrderKey], [OrderID], [StudentKey], [StudentID], [StudentName], [Session_1], [Session_2], [Session_3], [Session_4], [Session_5], [Session_6], [Session_7], [Session_8], [Session_9], [Session_10], [Session_11], [Session_12], [Session_13], [Session_14], [Session_15], [Session_16], [Session_17], [Session_18], [Session_19], [Session_20], [Description], [Slug], [RecordStatus], [CreatedBy], [CreatedName], [CreatedOn], [ModifiedBy], [ModifiedName], [ModifiedOn]) VALUES (2, 1, N'', N'1c0477bc-61ff-4160-bc13-f616c7a78558', N'1451010008', N'Nguyễn Hữu Đang', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, N'', N'', CAST(N'2019-09-09 00:28:50.820' AS DateTime), N'', N'', CAST(N'2019-09-10 09:04:52.823' AS DateTime))
INSERT [dbo].[HRM_Order_Study_Detail] ([AutoKey], [OrderKey], [OrderID], [StudentKey], [StudentID], [StudentName], [Session_1], [Session_2], [Session_3], [Session_4], [Session_5], [Session_6], [Session_7], [Session_8], [Session_9], [Session_10], [Session_11], [Session_12], [Session_13], [Session_14], [Session_15], [Session_16], [Session_17], [Session_18], [Session_19], [Session_20], [Description], [Slug], [RecordStatus], [CreatedBy], [CreatedName], [CreatedOn], [ModifiedBy], [ModifiedName], [ModifiedOn]) VALUES (3, 1, N'', N'82408315-f0ef-44f2-afaf-bfb5e5fd629f', N'1451010009', N'Nguyễn An Đông', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, N'', N'', CAST(N'2019-09-09 00:28:50.820' AS DateTime), N'', N'', CAST(N'2019-09-10 09:04:52.837' AS DateTime))
SET IDENTITY_INSERT [dbo].[HRM_Order_Study_Detail] OFF
SET IDENTITY_INSERT [dbo].[HRM_Order_Studys] ON 

INSERT [dbo].[HRM_Order_Studys] ([OrderKey], [OrderID], [OrderDate], [OrderQuit], [ClassKey], [ClassName], [StudyKey], [StudyName], [DepartmentKey], [DepartmentName], [CategoryKey], [CategoryName], [Slug], [RecordStatus], [OrderDescription], [CreatedBy], [CreatedName], [CreatedOn], [ModifiedBy], [ModifiedName], [ModifiedOn]) VALUES (1, N'', CAST(N'2019-09-09 00:28:50.800' AS DateTime), NULL, 1, N'14TH', 1, N'Tiếng anh', 1, N'Khoa công nghệ thông tin', 0, N'', 0, 1, N'', NULL, NULL, NULL, N'', N'', CAST(N'2019-09-09 00:28:50.803' AS DateTime))
INSERT [dbo].[HRM_Order_Studys] ([OrderKey], [OrderID], [OrderDate], [OrderQuit], [ClassKey], [ClassName], [StudyKey], [StudyName], [DepartmentKey], [DepartmentName], [CategoryKey], [CategoryName], [Slug], [RecordStatus], [OrderDescription], [CreatedBy], [CreatedName], [CreatedOn], [ModifiedBy], [ModifiedName], [ModifiedOn]) VALUES (2, NULL, CAST(N'2019-09-09 00:28:50.800' AS DateTime), NULL, 1, N'14YH', 2, N'Nhập môn', 1, N'Khoa công nghệ thông tin', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Order_Studys] ([OrderKey], [OrderID], [OrderDate], [OrderQuit], [ClassKey], [ClassName], [StudyKey], [StudyName], [DepartmentKey], [DepartmentName], [CategoryKey], [CategoryName], [Slug], [RecordStatus], [OrderDescription], [CreatedBy], [CreatedName], [CreatedOn], [ModifiedBy], [ModifiedName], [ModifiedOn]) VALUES (3, N'', CAST(N'2019-09-10 09:48:55.273' AS DateTime), NULL, 3, N'16TH', 3, N'Nhập môn lập trình', 1, N'Khoa kĩ thuật công nghệ', 0, N'', 0, 1, N'', N'', N'', CAST(N'2019-09-10 09:03:42.167' AS DateTime), N'', N'', CAST(N'2019-09-10 10:47:33.313' AS DateTime))
INSERT [dbo].[HRM_Order_Studys] ([OrderKey], [OrderID], [OrderDate], [OrderQuit], [ClassKey], [ClassName], [StudyKey], [StudyName], [DepartmentKey], [DepartmentName], [CategoryKey], [CategoryName], [Slug], [RecordStatus], [OrderDescription], [CreatedBy], [CreatedName], [CreatedOn], [ModifiedBy], [ModifiedName], [ModifiedOn]) VALUES (4, N'', CAST(N'2019-09-10 10:17:08.103' AS DateTime), NULL, 1, N'14TH', 1, N'Nhập môn tiếng anh', 1, N'Khoa kĩ thuật công nghệ', 0, N'', 0, 1, N'', N'', N'', CAST(N'2019-09-10 10:17:19.580' AS DateTime), N'', N'', CAST(N'2019-09-10 10:37:04.373' AS DateTime))
SET IDENTITY_INSERT [dbo].[HRM_Order_Studys] OFF
INSERT [dbo].[HRM_Student_Personals] ([PersonalKey], [PersonID], [FirstName], [LastName], [FullName], [StudentKey], [Gender], [Marriage], [BirthDay], [HomeTown], [IssueID], [IssueDate], [IssuePlace], [Description], [Phone], [Email], [BankCode], [AddressBank], [Slug], [RecordStatus], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (N'b6a069f5-1116-421b-9892-4872ff0f46fc', NULL, N'Đang', N'Nguyễn Hữu', N'Nguyễn Hữu Đang', N'1c0477bc-61ff-4160-bc13-f616c7a78558', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Student_Personals] ([PersonalKey], [PersonID], [FirstName], [LastName], [FullName], [StudentKey], [Gender], [Marriage], [BirthDay], [HomeTown], [IssueID], [IssueDate], [IssuePlace], [Description], [Phone], [Email], [BankCode], [AddressBank], [Slug], [RecordStatus], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (N'69158d92-adaf-4a9a-b5ad-9ed87b8f4c4d', NULL, N'Đông', N'Nguyễn An', N'Nguyễn An Đông', N'82408315-f0ef-44f2-afaf-bfb5e5fd629f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Students] ([StudentKey], [StudentID], [BranchKey], [BranchName], [DepartmentKey], [DepartmentName], [TeacherKey], [TeacherName], [PositionKey], [PositionName], [ClassKey], [ClassName], [BeginDate], [QuitDate], [Leaved], [Description], [Phone], [Email], [Slug], [Rank], [RecordStatus], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (N'82408315-f0ef-44f2-afaf-bfb5e5fd629f', N'1451010009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'14TH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Students] ([StudentKey], [StudentID], [BranchKey], [BranchName], [DepartmentKey], [DepartmentName], [TeacherKey], [TeacherName], [PositionKey], [PositionName], [ClassKey], [ClassName], [BeginDate], [QuitDate], [Leaved], [Description], [Phone], [Email], [Slug], [Rank], [RecordStatus], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (N'1c0477bc-61ff-4160-bc13-f616c7a78558', N'1451010008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'14TH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[HRM_Studys] ON 

INSERT [dbo].[HRM_Studys] ([StudyKey], [StudyID], [StudyName], [CategoryKey], [CategoryName], [Sesstion], [Credit], [Description], [Rank], [Slug], [RecordStatus], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1, N'NMTA', N'Nhập môn tiếng anh', NULL, NULL, 15, 20, N'15 buổi / 20 chỉ', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Studys] ([StudyKey], [StudyID], [StudyName], [CategoryKey], [CategoryName], [Sesstion], [Credit], [Description], [Rank], [Slug], [RecordStatus], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2, N'NMLT', N'Nhập môn lập trình', NULL, NULL, 15, 20, N'15 buổi / 20 chỉ', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Studys] ([StudyKey], [StudyID], [StudyName], [CategoryKey], [CategoryName], [Sesstion], [Credit], [Description], [Rank], [Slug], [RecordStatus], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (3, N'TA1', N'Tiếng anh 1', NULL, NULL, 15, 20, N'15 buổi / 20 chỉ', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Studys] ([StudyKey], [StudyID], [StudyName], [CategoryKey], [CategoryName], [Sesstion], [Credit], [Description], [Rank], [Slug], [RecordStatus], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (4, N'TA2', N'Tiếng anh 2', NULL, NULL, 15, 20, N'15 buổi / 20 chỉ', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[HRM_Studys] ([StudyKey], [StudyID], [StudyName], [CategoryKey], [CategoryName], [Sesstion], [Credit], [Description], [Rank], [Slug], [RecordStatus], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (5, N'TA3', N'Tiếng anh 3', NULL, NULL, 15, 20, N'15 buổi / 20 chỉ', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[HRM_Studys] OFF
USE [master]
GO
ALTER DATABASE [TN_Student] SET  READ_WRITE 
GO
